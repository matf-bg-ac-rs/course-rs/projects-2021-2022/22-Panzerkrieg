# Project Panzerkrieg

**Panzerkreig is a strategic multiplayer war game in which players either localy or through a network battle eachother with the goal of destroying the oponents army.**


## Table of contents 	
* [General info](#general-info-point_down)
* [Technologies](#technologies-wrench)
* [Setup](#setup-pencil2)
* [Developers](#developers-punch-muscle)

## General info 
**This is a multiplayer game, with a minimum of 2 and maximum of 4 players. The goal of the game is to defeat the oponents before they defeat you. At the start of every game, each player is given a tank and 7 tokens to use in the shop. The battle occurs by moving on the map, every army can move and attack once per turn. You are eliminated from the game if you run out of armies. The winner is the last standing player.**


## Technologies 
- **Qt i QtCreator - Version >= 5.15**
- **qmake - Version >= 5.12**
- **C++ - Version c++17**

## Setup 
Firstly it is neccessary to clone the repositorium and position in it:
```
$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/22-Panzerkrieg
$ cd 22-Panzerkrieg/
```

You can run the application in QtCreator, which is installed as follows:
```
sudo apt-get install build-essential
sudo apt-get install qtcreator
sudo apt-get install qt5-default
```

## Run local

Open Panzerkrieg.pro file in Qt Creator and press the green arrow run button on the bottom left of the screen.

## Run network

Firstly, open the Server.pro file in Qt Creator and press the run button. Secondly, open the Panzerkrieg.pro file in Qt Creator and run by pressing the aforementioned run button as many times as there are players.

## Demo video

[![Demo](../Resources/background.jpg)](https://drive.google.com/file/d/1dZT0A_KhzNrTOF9YFWd7urzGlsMZKMnc/view?usp=sharing)

## Developers

- [Jovan Marković, 69/2018](https://gitlab.com/j.markovic)
- [Tamara Miković, 82/2017](https://gitlab.com/t.mikovic)
- [Sara Stanković, 57/2018](https://gitlab.com/sara.stankovic1607)
- [Tina Mladenović, 116/2018](https://gitlab.com/tinamladenovic)
- [Miodrag Todorović, 100/2018](https://gitlab.com/m.todorovic)

