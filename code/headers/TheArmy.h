#ifndef THEARMY_H
#define THEARMY_H

#include <string>
#include <vector>
#include "Hex.h"

class TheArmy {
 public:
  virtual ~TheArmy() = default;

  TheArmy() = default;

  std::string getOwner() const;
  int getAttack() const;
  int getHealth() const;
  int getPrice() const;
  int getAttackReach() const;
  int getMaxMoveDistance() const;
  bool wasMoved() const;
  bool wasAttacking() const;

  virtual void specialAbility(Hex* hex) = 0;

  void setHealth(int health);
  void setOwner(std::string owner);
  void setAttackStrength(int strength);
  void setMaxMoveDistance(int distance);
  void setAttackReach(int reach);

  int attack(TheArmy& defArmy,
             bool canFightBack);  // 0 if nobody died, 1 if defArmy died, 2 if
                                  // caller died
  void setMoved(bool moved);
  void setAttacked(bool attacked);
  void resetAttAndMoveStats();
  int attackNetwork(TheArmy& defArmy,
                    bool canFightBack,
                    double a,
                    double b,
                    double c,
                    double d);

 protected:
  std::string _owner;
  int _attackStrength;
  int _maxMoveDistance;
  int _health;
  int _attackReach;
  int _price;
  bool _moved = false;
  bool _attacked = false;
};
#endif  // THEARMY_H
