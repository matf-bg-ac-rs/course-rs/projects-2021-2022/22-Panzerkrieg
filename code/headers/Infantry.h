#ifndef INFANTRY_H
#define INFANTRY_H

#include "TheArmy.h"

class Infantry : public TheArmy {
 public:
  Infantry(std::string owner);
  ~Infantry();

  void specialAbility(Hex* hex) override;
};
#endif  // INFANTRY_H
