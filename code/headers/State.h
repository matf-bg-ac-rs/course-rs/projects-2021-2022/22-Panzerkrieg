#ifndef STATE_H
#define STATE_H

#include <vector>
#include "Player.h"
#include "field.h"

class State {
 public:
  Player* currentPlayer;
  Field* selectField;
  std::vector<Field*> legalMoves;
  std::vector<Field*> legalAttacks;

  void resetFields();
};

#endif  // STATE_H
