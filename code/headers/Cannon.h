#ifndef CANNON_H
#define CANNON_H

#include "TheArmy.h"

class Cannon : public TheArmy {
 public:
  Cannon(std::string owner);
  ~Cannon();

  void specialAbility(Hex* hex) override;
};

#endif  // CANNON_H
