#ifndef SHOPWINDOW_H
#define SHOPWINDOW_H

#include <QWidget>

namespace Ui {
class shopwindow;
}

class shopwindow : public QWidget {
  Q_OBJECT

 public:
  explicit shopwindow(QWidget* parent = nullptr);
  ~shopwindow();
  void setupWindow();
  void buyArmy(std::string);

  void resizeEvent(QResizeEvent* event);
  Ui::shopwindow* getUI();

 public slots:

  void on_infantry_clicked();

  void on_tank_clicked();

  void on_cavalry_clicked();

  void on_cannon_clicked();

  void on_exit_clicked();

 private:
  Ui::shopwindow* ui;
};

#endif  // SHOPWINDOW_H
