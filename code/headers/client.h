#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QSignalMapper>
#include <QTcpSocket>
#include "field.h"

class Client : public QObject {
  Q_OBJECT

 public:
  Client();
  ~Client();
  std::string getPlayer();
  void hexClicked(std::string);

 signals:
  void wrongNumPlayers();
  void start(const QString&);
  void startGame(const QString&,
                 const QString&,
                 const QString&,
                 const QString&,
                 const QString&,
                 const QString&);
  void endTurn(const QString&);
  void endForAll();
  void playerShopped(const QString&);
  void mapAfterShop(std::string army);
  void mapPressed(const QString&);
  void changeMap(int, int, double, double, double, double);
  void playerQuit(const QString&);
  void endGame(std::string);

 private:
  QTcpSocket* _socket;

  void readMessage(const QString& message);
  void readyRead();
  void sendToServer(const QString& text);
  std::string _player = "";
};

#endif  // CLIENT_H
