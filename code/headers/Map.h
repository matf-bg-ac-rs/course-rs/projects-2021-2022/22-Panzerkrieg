#ifndef MAP_HPP
#define MAP_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <queue>
#include <set>
#include <vector>
#include "HexBoard.h"

class Map {
 public:
  Map(std::string path);
  std::vector<Field*> getCities();
  ~Map();

  Field* getFieldFromCoord(int x, int y);
  int distance(Field* f1, Field* f2);
  std::vector<Field*> getLegalMoveFields(Field* f, int dist);
  std::vector<Field*> getLegalAttackFields(Field* f, int dist);
  void move(Field& field1, Field& field2);
  std::vector<Field*> getNeighbors(Field* f);
  HexBoard* getHexboard();

 private:
  HexBoard* hexboard;
  std::string _path;
  int cols_size, rows_size;

  void draw();
  void readMapFromFile(std::vector<std::vector<FieldType>>& types);

  std::vector<Field*> getNeighbors(int x, int y);
  std::vector<Field*> BFS(Field* v, int dist, bool (*cond)(Field*));
};

#endif  // MAP_HPP
