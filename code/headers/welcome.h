#ifndef WELCOME_H
#define WELCOME_H

#include <QWidget>

namespace Ui {
class welcome;
}

class welcome : public QWidget {
  Q_OBJECT

 public:
  explicit welcome(QWidget* parent = nullptr);
  ~welcome();

  void resizeEvent(QResizeEvent* event);
  Ui::welcome* getUI();
  void startGame(std::string player, std::string path, std::vector<int> cities);
  bool getNetwork();

 signals:
  void setupClient();

 private slots:

  void on_start_game_clicked();

  void on_network_clicked();

  void on_local_clicked();

  void on_radioButton_3_clicked();

  void on_radioButton_2_clicked();

  void on_radioButton_clicked();

 private:
  Ui::welcome* ui;
  bool play_on_network = false;
};

#endif  // WELCOME_H
