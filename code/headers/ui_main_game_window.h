/********************************************************************************
** Form generated from reading UI file 'main_game_window.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAIN_GAME_WINDOW_H
#define UI_MAIN_GAME_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainGameWindow {
 public:
  QHBoxLayout* horizontalLayout_4;
  QVBoxLayout* verticalLayout_2;
  QHBoxLayout* horizontalLayout;
  QLabel* label_3;
  QLabel* player_id;
  QGraphicsView* graphicsView;
  QHBoxLayout* horizontalLayout_2;
  QLabel* label_7;
  QSpacerItem* horizontalSpacer;
  QLabel* label_6;
  QVBoxLayout* verticalLayout;
  QPushButton* end_turn;
  QPushButton* shop;
  QPushButton* exit;
  QButtonGroup* buttonGroup;

  void setupUi(QWidget* MainGameWindow) {
    if (MainGameWindow->objectName().isEmpty())
      MainGameWindow->setObjectName(QString::fromUtf8("MainGameWindow"));
    MainGameWindow->resize(800, 600);
    MainGameWindow->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}"));
    horizontalLayout_4 = new QHBoxLayout(MainGameWindow);
    horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
    verticalLayout_2 = new QVBoxLayout();
    verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    label_3 = new QLabel(MainGameWindow);
    label_3->setObjectName(QString::fromUtf8("label_3"));

    horizontalLayout->addWidget(label_3);

    player_id = new QLabel(MainGameWindow);
    player_id->setObjectName(QString::fromUtf8("player_id"));
    player_id->setAlignment(Qt::AlignRight | Qt::AlignTrailing |
                            Qt::AlignVCenter);

    horizontalLayout->addWidget(player_id);

    verticalLayout_2->addLayout(horizontalLayout);

    graphicsView = new QGraphicsView(MainGameWindow);
    graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

    verticalLayout_2->addWidget(graphicsView);

    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    label_7 = new QLabel(MainGameWindow);
    label_7->setObjectName(QString::fromUtf8("label_7"));

    horizontalLayout_2->addWidget(label_7);

    horizontalSpacer =
        new QSpacerItem(18, 13, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer);

    label_6 = new QLabel(MainGameWindow);
    label_6->setObjectName(QString::fromUtf8("label_6"));

    horizontalLayout_2->addWidget(label_6);

    verticalLayout_2->addLayout(horizontalLayout_2);

    horizontalLayout_4->addLayout(verticalLayout_2);

    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    end_turn = new QPushButton(MainGameWindow);
    buttonGroup = new QButtonGroup(MainGameWindow);
    buttonGroup->setObjectName(QString::fromUtf8("buttonGroup"));
    buttonGroup->addButton(end_turn);
    end_turn->setObjectName(QString::fromUtf8("end_turn"));

    verticalLayout->addWidget(end_turn);

    shop = new QPushButton(MainGameWindow);
    buttonGroup->addButton(shop);
    shop->setObjectName(QString::fromUtf8("shop"));

    verticalLayout->addWidget(shop);

    exit = new QPushButton(MainGameWindow);
    buttonGroup->addButton(exit);
    exit->setObjectName(QString::fromUtf8("exit"));

    verticalLayout->addWidget(exit);

    horizontalLayout_4->addLayout(verticalLayout);

    retranslateUi(MainGameWindow);

    QMetaObject::connectSlotsByName(MainGameWindow);
  }  // setupUi

  void retranslateUi(QWidget* MainGameWindow) {
    MainGameWindow->setWindowTitle(
        QCoreApplication::translate("MainGameWindow", "Panzerkrieg", nullptr));
    label_3->setText(QString());
    player_id->setText(QString());
    label_7->setText(QString());
    label_6->setText(QString());
    end_turn->setText(
        QCoreApplication::translate("MainGameWindow", "End Turn", nullptr));
    shop->setText(
        QCoreApplication::translate("MainGameWindow", "SHOP", nullptr));
    exit->setText(
        QCoreApplication::translate("MainGameWindow", "EXIT", nullptr));
  }  // retranslateUi
};

namespace Ui {
class MainGameWindow : public Ui_MainGameWindow {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_MAIN_GAME_WINDOW_H
