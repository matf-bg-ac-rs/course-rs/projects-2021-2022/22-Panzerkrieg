#ifndef HEX_H
#define HEX_H

#include <QGraphicsPolygonItem>
#include <QObject>

enum FieldType { Forest = 'f', Clear = 'c', Mountain = 'm', City = 'a' };

enum FieldState { None, Move, Attack, Selected, SelectedShop };

class Hex : public QGraphicsPolygonItem {
 public:
  Hex(int x, int y, FieldType type, QGraphicsItem* parent = NULL);
  int getScale();
  FieldType getType();

  // void setType(FieldType type, int, int);

  QRectF boundingRect() const override;
  void paint(QPainter* painter,
             const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override;
  void setState(FieldState state);
  void placeArmy(int x, int y, double a, double b, double c, double d);

 private:
  int _scale = 30;
  FieldType _hexType;  // TODO fix this naming
  int _x, _y;
  FieldState _state = FieldState::None;

  const QPointF _points[6] = {
      QPointF(1.0, 0.0) * _scale, QPointF(2.0, 0.0) * _scale,
      QPointF(3.0, 1.0) * _scale, QPointF(2.0, 2.0) * _scale,
      QPointF(1.0, 2.0) * _scale, QPointF(0.0, 1.0) * _scale,
  };

  void drawImage(QPainter*);
  void drawFrame(QPainter*);
  void drawArmy(QPainter*);
};

#endif  // HEX_H
