#ifndef PLAYER_H
#define PLAYER_H

#include <set>
#include "TheArmy.h"
#include "field.h"
class Player {
 public:
  Player(std::string name, int money, Qt::GlobalColor color);
  // bool buy(TheArmy army);
  std::string getName();
  int getMoney();
  Qt::GlobalColor getColor();
  std::set<TheArmy*> getArmy();
  std::set<Field*> getCities();
  void subtractMoney(TheArmy& army);
  void addArmy(TheArmy& new_army);
  void addMoney(int money);
  void setTurn(bool turn);
  void addCity(Field& city);
  void removeCity(Field& city);
  void removeArmy(TheArmy& army);
  bool getTurn();

 private:
  std::string _name;
  std::set<TheArmy*> _army;
  std::set<Field*> _cities;
  int _money;
  Qt::GlobalColor color;
  bool _turn = false;
};

#endif  // PLAYER_H
