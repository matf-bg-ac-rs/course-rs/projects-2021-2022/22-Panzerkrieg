/********************************************************************************
** Form generated from reading UI file 'shopwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOPWINDOW_H
#define UI_SHOPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_shopwindow {
 public:
  QHBoxLayout* horizontalLayout_8;
  QVBoxLayout* verticalLayout;
  QLabel* available_money;
  QSpacerItem* verticalSpacer;
  QGridLayout* gridLayout_2;
  QLabel* label;
  QPushButton* cavalry;
  QLabel* label_2;
  QPushButton* cannon;
  QLabel* label_3;
  QLabel* label_4;
  QPushButton* infantry;
  QLabel* label_5;
  QPushButton* tank;
  QLabel* label_7;
  QLabel* label_6;
  QSpacerItem* verticalSpacer_2;
  QPushButton* exit;
  QSpacerItem* verticalSpacer_3;

  void setupUi(QWidget* shopwindow) {
    if (shopwindow->objectName().isEmpty())
      shopwindow->setObjectName(QString::fromUtf8("shopwindow"));
    shopwindow->resize(755, 558);
    shopwindow->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27;\n"
                          "background-image: url(:/images/background.jpg);\n"
                          "    }\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}\n"
                          "\n"
                          ""));
    horizontalLayout_8 = new QHBoxLayout(shopwindow);
    horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    available_money = new QLabel(shopwindow);
    available_money->setObjectName(QString::fromUtf8("available_money"));
    available_money->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(available_money);

    verticalSpacer =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer);

    gridLayout_2 = new QGridLayout();
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    label = new QLabel(shopwindow);
    label->setObjectName(QString::fromUtf8("label"));
    label->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label, 3, 1, 1, 1);

    cavalry = new QPushButton(shopwindow);
    cavalry->setObjectName(QString::fromUtf8("cavalry"));

    gridLayout_2->addWidget(cavalry, 3, 0, 1, 1);

    label_2 = new QLabel(shopwindow);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label_2, 1, 1, 1, 1);

    cannon = new QPushButton(shopwindow);
    cannon->setObjectName(QString::fromUtf8("cannon"));

    gridLayout_2->addWidget(cannon, 4, 0, 1, 1);

    label_3 = new QLabel(shopwindow);
    label_3->setObjectName(QString::fromUtf8("label_3"));
    label_3->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label_3, 2, 1, 1, 1);

    label_4 = new QLabel(shopwindow);
    label_4->setObjectName(QString::fromUtf8("label_4"));
    label_4->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label_4, 5, 1, 1, 1);

    infantry = new QPushButton(shopwindow);
    infantry->setObjectName(QString::fromUtf8("infantry"));

    gridLayout_2->addWidget(infantry, 2, 0, 1, 1);

    label_5 = new QLabel(shopwindow);
    label_5->setObjectName(QString::fromUtf8("label_5"));
    label_5->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label_5, 1, 0, 1, 1);

    tank = new QPushButton(shopwindow);
    tank->setObjectName(QString::fromUtf8("tank"));

    gridLayout_2->addWidget(tank, 5, 0, 1, 1);

    label_7 = new QLabel(shopwindow);
    label_7->setObjectName(QString::fromUtf8("label_7"));
    label_7->setAlignment(Qt::AlignCenter);

    gridLayout_2->addWidget(label_7, 4, 1, 1, 1);

    verticalLayout->addLayout(gridLayout_2);

    label_6 = new QLabel(shopwindow);
    label_6->setObjectName(QString::fromUtf8("label_6"));
    label_6->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label_6);

    verticalSpacer_2 =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer_2);

    exit = new QPushButton(shopwindow);
    exit->setObjectName(QString::fromUtf8("exit"));

    verticalLayout->addWidget(exit);

    verticalSpacer_3 =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer_3);

    horizontalLayout_8->addLayout(verticalLayout);

    retranslateUi(shopwindow);

    QMetaObject::connectSlotsByName(shopwindow);
  }  // setupUi

  void retranslateUi(QWidget* shopwindow) {
    shopwindow->setWindowTitle(
        QCoreApplication::translate("shopwindow", "Panzerkrieg", nullptr));
    available_money->setText(
        QCoreApplication::translate("shopwindow", "Availabe Money", nullptr));
    label->setText(QCoreApplication::translate("shopwindow", "2", nullptr));
    cavalry->setText(
        QCoreApplication::translate("shopwindow", "Cavalry", nullptr));
    label_2->setText(
        QCoreApplication::translate("shopwindow", "Army Price", nullptr));
    cannon->setText(
        QCoreApplication::translate("shopwindow", "Cannon", nullptr));
    label_3->setText(QCoreApplication::translate("shopwindow", "1", nullptr));
    label_4->setText(QCoreApplication::translate("shopwindow", "4", nullptr));
    infantry->setText(
        QCoreApplication::translate("shopwindow", "Infantry", nullptr));
    label_5->setText(
        QCoreApplication::translate("shopwindow", "Army Type", nullptr));
    tank->setText(QCoreApplication::translate("shopwindow", "Tank", nullptr));
    label_7->setText(QCoreApplication::translate("shopwindow", "3", nullptr));
    label_6->setText(QString());
    exit->setText(QCoreApplication::translate("shopwindow", "DONE", nullptr));
  }  // retranslateUi
};

namespace Ui {
class shopwindow : public Ui_shopwindow {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_SHOPWINDOW_H
