#ifndef EXIT_H
#define EXIT_H

#include <QWidget>

namespace Ui {
class Exit;
}

class Exit : public QWidget {
  Q_OBJECT

 public:
  explicit Exit(QWidget* parent = nullptr);
  ~Exit();

  void resizeEvent(QResizeEvent* event);

 signals:
  void isClosing();

 private slots:
  void on_yes_clicked();
  void on_no_clicked();

 private:
  Ui::Exit* ui;
};

#endif  // EXIT_H
