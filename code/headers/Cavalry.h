#ifndef CAVALRY_H
#define CAVALRY_H

#include <vector>
#include "TheArmy.h"

class Cavalry : public TheArmy {
 public:
  Cavalry(std::string owner);
  ~Cavalry();

  void specialAbility(Hex* hex) override;
};
#endif  // CAVALRY_H
