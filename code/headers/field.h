#ifndef FIELD_H
#define FIELD_H

#include <QGraphicsItem>
#include <QGraphicsPolygonItem>
#include <QMouseEvent>
#include <iostream>
#include "Hex.h"
#include "TheArmy.h"

class Field {
 public:
  ~Field();
  Field(const Field& other);
  Field& operator=(const Field& other);
  Field(int x, int y, FieldType type);

  bool isOccupied() const;
  FieldType getType() const;
  std::string getOwner() const;
  void setOccupied(bool occupied);
  void setOwner(std::string newOwner);  // TODO change this to enum
  void deinitField();
  void initField(std::string owner, TheArmy* army);
  void setArmy(TheArmy* army);
  TheArmy* getArmy();

  Hex* getHex();
  int getX() const;
  int getY() const;

  friend bool operator==(const Field& f1, const Field& f2);

 private:
  Hex* hex;
  int _x, _y;
  FieldType _type;
  bool _occupied = false;
  std::string _owner = "";
  TheArmy* _placedArmy;
};

#endif  // FIELD_H
