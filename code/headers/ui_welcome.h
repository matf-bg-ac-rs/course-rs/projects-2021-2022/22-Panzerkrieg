/********************************************************************************
** Form generated from reading UI file 'welcome.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WELCOME_H
#define UI_WELCOME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_welcome {
 public:
  QVBoxLayout* verticalLayout;
  QSpacerItem* verticalSpacer;
  QLabel* label;
  QSpacerItem* verticalSpacer_2;
  QHBoxLayout* horizontalLayout_3;
  QSpacerItem* horizontalSpacer_5;
  QRadioButton* local;
  QRadioButton* network;
  QSpacerItem* horizontalSpacer_6;
  QLabel* label_2;
  QHBoxLayout* horizontalLayout;
  QSpacerItem* horizontalSpacer_3;
  QRadioButton* radioButton_3;
  QRadioButton* radioButton_2;
  QRadioButton* radioButton;
  QSpacerItem* horizontalSpacer_4;
  QHBoxLayout* horizontalLayout_2;
  QSpacerItem* horizontalSpacer;
  QPushButton* start_game;
  QSpacerItem* horizontalSpacer_2;
  QLabel* player_warning;
  QLabel* warning;
  QButtonGroup* networkGroup;

  void setupUi(QWidget* welcome) {
    if (welcome->objectName().isEmpty())
      welcome->setObjectName(QString::fromUtf8("welcome"));
    welcome->resize(320, 240);
    welcome->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "QRadioButton{\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}"));
    verticalLayout = new QVBoxLayout(welcome);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    verticalSpacer =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer);

    label = new QLabel(welcome);
    label->setObjectName(QString::fromUtf8("label"));
    QFont font;
    font.setFamilies({QString::fromUtf8("Cooper Black")});
    font.setBold(true);
    label->setFont(font);
    label->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label);

    verticalSpacer_2 =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer_2);

    horizontalLayout_3 = new QHBoxLayout();
    horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
    horizontalSpacer_5 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer_5);

    local = new QRadioButton(welcome);
    networkGroup = new QButtonGroup(welcome);
    networkGroup->setObjectName(QString::fromUtf8("networkGroup"));
    networkGroup->addButton(local);
    local->setObjectName(QString::fromUtf8("local"));

    horizontalLayout_3->addWidget(local);

    network = new QRadioButton(welcome);
    networkGroup->addButton(network);
    network->setObjectName(QString::fromUtf8("network"));

    horizontalLayout_3->addWidget(network);

    horizontalSpacer_6 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_3->addItem(horizontalSpacer_6);

    verticalLayout->addLayout(horizontalLayout_3);

    label_2 = new QLabel(welcome);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setTextFormat(Qt::AutoText);
    label_2->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label_2);

    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    horizontalSpacer_3 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout->addItem(horizontalSpacer_3);

    radioButton_3 = new QRadioButton(welcome);
    radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
    radioButton_3->setEnabled(true);
    radioButton_3->setCheckable(false);

    horizontalLayout->addWidget(radioButton_3);

    radioButton_2 = new QRadioButton(welcome);
    radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
    radioButton_2->setCheckable(false);

    horizontalLayout->addWidget(radioButton_2);

    radioButton = new QRadioButton(welcome);
    radioButton->setObjectName(QString::fromUtf8("radioButton"));
    radioButton->setCheckable(false);

    horizontalLayout->addWidget(radioButton);

    horizontalSpacer_4 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout->addItem(horizontalSpacer_4);

    verticalLayout->addLayout(horizontalLayout);

    horizontalLayout_2 = new QHBoxLayout();
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    horizontalSpacer =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer);

    start_game = new QPushButton(welcome);
    start_game->setObjectName(QString::fromUtf8("start_game"));

    horizontalLayout_2->addWidget(start_game);

    horizontalSpacer_2 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout_2->addItem(horizontalSpacer_2);

    verticalLayout->addLayout(horizontalLayout_2);

    player_warning = new QLabel(welcome);
    player_warning->setObjectName(QString::fromUtf8("player_warning"));
    player_warning->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(player_warning);

    warning = new QLabel(welcome);
    warning->setObjectName(QString::fromUtf8("warning"));
    warning->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(warning);

    retranslateUi(welcome);

    QMetaObject::connectSlotsByName(welcome);
  }  // setupUi

  void retranslateUi(QWidget* welcome) {
    welcome->setWindowTitle(
        QCoreApplication::translate("welcome", "Panzerkrieg", nullptr));
    label->setText(
        QCoreApplication::translate("welcome", "PanzerKrieg", nullptr));
    local->setText(QCoreApplication::translate("welcome", "Local", nullptr));
    network->setText(
        QCoreApplication::translate("welcome", "Network", nullptr));
    label_2->setText(QCoreApplication::translate(
        "welcome", "Choose number of players", nullptr));
    radioButton_3->setText(
        QCoreApplication::translate("welcome", "2", nullptr));
    radioButton_2->setText(
        QCoreApplication::translate("welcome", "3", nullptr));
    radioButton->setText(QCoreApplication::translate("welcome", "4", nullptr));
    start_game->setText(
        QCoreApplication::translate("welcome", "Start Game", nullptr));
    player_warning->setText(QString());
    warning->setText(QString());
  }  // retranslateUi
};

namespace Ui {
class welcome : public Ui_welcome {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_WELCOME_H
