#ifndef MAIN_GAME_WINDOW_H
#define MAIN_GAME_WINDOW_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include "Player.h"
#include "exit.h"
#include "shopwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainGameWindow;
}
QT_END_NAMESPACE

class MainGameWindow : public QWidget {
  Q_OBJECT

 public:
  MainGameWindow(QWidget* parent = nullptr);
  ~MainGameWindow();

  Exit* getExitWindow();

  void resizeEvent(QResizeEvent* event);

  QGraphicsScene* scene;
  void draw();
  void setPlayer(Player& p);
  void placeArmy(std::string text, TheArmy* army);
  TheArmy* getArmy();
  void resetWindow();
  Ui::MainGameWindow* getUI();
  void endTurn();
  shopwindow* getShop();

 public slots:

  void on_end_turn_clicked();

  void on_shop_clicked();

  void on_exit_clicked();

 private:
  Ui::MainGameWindow* ui;
  TheArmy* army = nullptr;
  Exit* exit_window;
  shopwindow* shop_window;
};
#endif  // MAIN_GAME_WINDOW_H
