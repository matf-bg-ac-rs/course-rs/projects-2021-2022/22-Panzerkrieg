#ifndef GAME_H
#define GAME_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <string>
#include <vector>
#include "HexBoard.h"
#include "Map.h"
#include "Player.h"
#include "State.h"
#include "client.h"
#include "field.h"
#include "main_game_window.h"
#include "playerlost.h"
#include "shopwindow.h"
#include "welcome.h"
#include "winner.h"

class Game : public QGraphicsView {
 public:
  // constructors
  Game(QWidget* parent = NULL);
  ~Game();

  // public methods
  void start(std::string path, std::vector<int> cities);
  void initializePlayersAndResources(int numOfPlayers,
                                     std::vector<int> indexCities);
  void declareWinner(Player& player);
  void addMoneyForCities(Player& player);
  void initializeTurn(Player& player);
  void checkPlayers();
  Player* getCurrentPlayer();
  Player* getPlayerByName(std::string name);
  void deletePlayer(Player& player);
  void setNextPlayer();
  Player* getNextPlayer();
  void setNumberOfPlayers(int num);
  int getNumberOfPlayers();
  MainGameWindow* getMainGameWindow();
  welcome* getWelcomeWindow();
  Client* getClient();
  Qt::GlobalColor getColor(int i);
  Winner* getWinnerWindow();

  Map* map;
  int numOfPlayers = 4;
  std::vector<Player*> players;
  State* state = new State();

  // use in Hex::mousePressEvent
  void updateShop(Field* f);
  void showAvailableFields(Field* f);
  void MakeMoveOrAttack(Field* f, double a, double b, double c, double d);

 private:
  MainGameWindow* main_game_window;
  welcome* welcome_window;
  Winner* winner_window;
  PlayerLost* player_lost;
  Client* client;
};

#endif  // GAME_H
