#ifndef TANK_HPP
#define TANK_HPP

#include "TheArmy.h"

class Tank : public TheArmy {
 public:
  Tank(std::string owner);
  ~Tank();

  void specialAbility(Hex* hex) override;
};
#endif  // TANK_HPP
