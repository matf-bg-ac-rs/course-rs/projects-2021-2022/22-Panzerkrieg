/********************************************************************************
** Form generated from reading UI file 'exit.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXIT_H
#define UI_EXIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Exit {
 public:
  QVBoxLayout* verticalLayout;
  QLabel* label;
  QHBoxLayout* horizontalLayout;
  QPushButton* yes;
  QPushButton* no;

  void setupUi(QWidget* Exit) {
    if (Exit->objectName().isEmpty())
      Exit->setObjectName(QString::fromUtf8("Exit"));
    Exit->resize(320, 240);
    Exit->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}"));
    verticalLayout = new QVBoxLayout(Exit);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    label = new QLabel(Exit);
    label->setObjectName(QString::fromUtf8("label"));
    label->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label);

    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    yes = new QPushButton(Exit);
    yes->setObjectName(QString::fromUtf8("yes"));

    horizontalLayout->addWidget(yes);

    no = new QPushButton(Exit);
    no->setObjectName(QString::fromUtf8("no"));

    horizontalLayout->addWidget(no);

    verticalLayout->addLayout(horizontalLayout);

    retranslateUi(Exit);

    QMetaObject::connectSlotsByName(Exit);
  }  // setupUi

  void retranslateUi(QWidget* Exit) {
    Exit->setWindowTitle(QCoreApplication::translate("Exit", "Exit", nullptr));
    label->setText(QCoreApplication::translate(
        "Exit", "Are you sure you wan't to quit the game?", nullptr));
    yes->setText(QCoreApplication::translate("Exit", "Yes", nullptr));
    no->setText(QCoreApplication::translate("Exit", "No", nullptr));
  }  // retranslateUi
};

namespace Ui {
class Exit : public Ui_Exit {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_EXIT_H
