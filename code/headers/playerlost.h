#ifndef PLAYERLOST_H
#define PLAYERLOST_H

#include <QWidget>

namespace Ui {
class PlayerLost;
}

class PlayerLost : public QWidget {
  Q_OBJECT

 public:
  explicit PlayerLost(QWidget* parent = nullptr);
  ~PlayerLost();

  void setLabel(QString text);
  void resizeEvent(QResizeEvent* event);

 private slots:
  void on_button_ok_clicked();

 private:
  Ui::PlayerLost* ui;
};

#endif  // PLAYERLOST_H
