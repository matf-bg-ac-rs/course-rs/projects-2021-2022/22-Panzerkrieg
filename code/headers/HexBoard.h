#ifndef HEXBOARD_H
#define HEXBOARD_H

#include <QList>
#include <vector>
#include "field.h"

class HexBoard {
 public:
  HexBoard();
  std::vector<std::vector<Field*>> fields;
  ~HexBoard();

  // public methods
  void placeHexes(int x,
                  int y,
                  int cols,
                  int rows,
                  std::vector<std::vector<FieldType>>& types);
  void addCity(Field* field);
  std::vector<Field*> getCities();

 private:
  void createHexColumn(int x,
                       int y,
                       int numOfRows,
                       int y_index,
                       int x_off,
                       std::vector<FieldType>& types);

  std::vector<Field*> cities;
};

#endif  // HEXBOARD_H
