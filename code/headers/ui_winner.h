/********************************************************************************
** Form generated from reading UI file 'winner.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINNER_H
#define UI_WINNER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Winner {
 public:
  QVBoxLayout* verticalLayout;
  QSpacerItem* verticalSpacer_2;
  QLabel* label;
  QSpacerItem* verticalSpacer;
  QHBoxLayout* horizontalLayout;
  QSpacerItem* horizontalSpacer_2;
  QPushButton* end_game;
  QSpacerItem* horizontalSpacer;

  void setupUi(QWidget* Winner) {
    if (Winner->objectName().isEmpty())
      Winner->setObjectName(QString::fromUtf8("Winner"));
    Winner->resize(320, 240);
    Winner->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "QRadioButton{\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}"));
    verticalLayout = new QVBoxLayout(Winner);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    verticalSpacer_2 =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer_2);

    label = new QLabel(Winner);
    label->setObjectName(QString::fromUtf8("label"));
    label->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label);

    verticalSpacer =
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer);

    horizontalLayout = new QHBoxLayout();
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    horizontalSpacer_2 =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout->addItem(horizontalSpacer_2);

    end_game = new QPushButton(Winner);
    end_game->setObjectName(QString::fromUtf8("end_game"));

    horizontalLayout->addWidget(end_game);

    horizontalSpacer =
        new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    horizontalLayout->addItem(horizontalSpacer);

    verticalLayout->addLayout(horizontalLayout);

    retranslateUi(Winner);

    QMetaObject::connectSlotsByName(Winner);
  }  // setupUi

  void retranslateUi(QWidget* Winner) {
    Winner->setWindowTitle(
        QCoreApplication::translate("Winner", "Form", nullptr));
    label->setText(QCoreApplication::translate(
        "Winner", "The winner is xxxxxxx", nullptr));
    end_game->setText(
        QCoreApplication::translate("Winner", "End Game", nullptr));
  }  // retranslateUi
};

namespace Ui {
class Winner : public Ui_Winner {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_WINNER_H
