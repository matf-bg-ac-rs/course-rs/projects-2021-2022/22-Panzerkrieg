/********************************************************************************
** Form generated from reading UI file 'playerlost.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYERLOST_H
#define UI_PLAYERLOST_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlayerLost {
 public:
  QVBoxLayout* verticalLayout;
  QLabel* label;
  QPushButton* button_ok;

  void setupUi(QWidget* PlayerLost) {
    if (PlayerLost->objectName().isEmpty())
      PlayerLost->setObjectName(QString::fromUtf8("PlayerLost"));
    PlayerLost->resize(320, 240);
    PlayerLost->setStyleSheet(
        QString::fromUtf8("QLabel {\n"
                          "        font-weight: bold;\n"
                          "        color : #f2ab27; \n"
                          " }\n"
                          "\n"
                          "\n"
                          "QPushButton{\n"
                          " font-weight: bold;\n"
                          " color: #f2ab27; \n"
                          "background-color: rgba(0, 0, 0, 0.5);\n"
                          " border: 3px solid black;\n"
                          "background-image: url(:/images/background.jpg);}"));
    verticalLayout = new QVBoxLayout(PlayerLost);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    label = new QLabel(PlayerLost);
    label->setObjectName(QString::fromUtf8("label"));
    label->setAlignment(Qt::AlignCenter);

    verticalLayout->addWidget(label);

    button_ok = new QPushButton(PlayerLost);
    button_ok->setObjectName(QString::fromUtf8("button_ok"));

    verticalLayout->addWidget(button_ok);

    retranslateUi(PlayerLost);

    QMetaObject::connectSlotsByName(PlayerLost);
  }  // setupUi

  void retranslateUi(QWidget* PlayerLost) {
    PlayerLost->setWindowTitle(
        QCoreApplication::translate("PlayerLost", "Form", nullptr));
    label->setText(
        QCoreApplication::translate("PlayerLost", "TextLabel", nullptr));
    button_ok->setText(
        QCoreApplication::translate("PlayerLost", "OK", nullptr));
  }  // retranslateUi
};

namespace Ui {
class PlayerLost : public Ui_PlayerLost {};
}  // namespace Ui

QT_END_NAMESPACE

#endif  // UI_PLAYERLOST_H
