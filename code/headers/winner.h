#ifndef WINNER_H
#define WINNER_H

#include <QWidget>

namespace Ui {
class Winner;
}

class Winner : public QWidget {
  Q_OBJECT

 public:
  explicit Winner(QWidget* parent = nullptr);
  ~Winner();

  void setWinner(const QString text);

  void resizeEvent(QResizeEvent* event);
  void closeEvent(QCloseEvent* event);
  void endGameNetwork(const QString text);

 signals:
  void isClosing();

 public slots:

  void on_end_game_clicked();

 private:
  Ui::Winner* ui;
};

#endif  // WINNER_H
