#ifndef SHOP_H
#define SHOP_H

#include "Player.h"
#include "TheArmy.h"

#include <unordered_map>

enum ArmyType { infantry = 0, cavalry = 1, cannon = 2, tank = 3 };

class Shop {
 public:
  Shop();

  static bool buy(Player* p, TheArmy* army);

  static void setArmyPrice(int infantry_price,
                           int cavalry_price,
                           int tank_price,
                           int cannon_price);

  static int getArmyPrice(ArmyType a);

 private:
  static std::unordered_map<ArmyType, int> _army_price;
};

#endif  // SHOP_H
