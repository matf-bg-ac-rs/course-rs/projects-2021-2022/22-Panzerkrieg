#include "../headers/Player.h"
#include "../headers/Game.h"

Player::Player(std::string name, int money, Qt::GlobalColor color)
    : _name(name), _money(money), color(color){};

std::string Player::getName() {
  return this->_name;
}

int Player::getMoney() {
  return this->_money;
}

Qt::GlobalColor Player::getColor() {
  return this->color;
}

void Player::addMoney(int money) {
  this->_money += money;
}

void Player::setTurn(bool turn) {
  this->_turn = turn;
}

void Player::addCity(Field& city) {
  city.setOccupied(true);
  city.setOwner(this->getName());
  this->_cities.insert(&city);
}

void Player::removeCity(Field& city) {
  _cities.erase(&city);
}

void Player::removeArmy(TheArmy& army) {
  _army.erase(&army);
}

void Player::addArmy(TheArmy& new_army) {
  this->_army.insert(&new_army);
}

void Player::subtractMoney(TheArmy& army) {
  this->_money -= army.getPrice();
}

std::set<TheArmy*> Player::getArmy() {
  return this->_army;
}

std::set<Field*> Player::getCities() {
  return this->_cities;
}

bool Player::getTurn() {
  return _turn;
}
