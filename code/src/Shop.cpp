#include "../headers/Shop.h"

Shop::Shop() {}

bool Shop::buy(Player* p, TheArmy* army) {
  if (army->getPrice() > p->getMoney()) {
    return false;
  }

  p->addArmy(*army);

  return true;
}

void Shop::setArmyPrice(int infantry_price,
                        int cavalry_price,
                        int tank_price,
                        int cannon_price) {
  _army_price[ArmyType(infantry)] = infantry_price;
  _army_price[ArmyType(cavalry)] = cavalry_price;
  _army_price[ArmyType(tank)] = tank_price;
  _army_price[ArmyType(cannon)] = cannon_price;
}

int Shop::getArmyPrice(ArmyType a) {
  return _army_price[a];
}

std::unordered_map<ArmyType, int> Shop::_army_price;
