#include "../headers/Game.h"
#include <QRandomGenerator>
#include <Qt>
#include <iostream>
#include <random>
#include "../headers/HexBoard.h"
#include "../headers/Map.h"
#include "../headers/Shop.h"
#include "../headers/Tank.h"  //TODO fix this to take TheArmy class

Game::Game(QWidget* parent) {
  Q_UNUSED(parent)

  main_game_window = new MainGameWindow();
  main_game_window->setWindowFlags(Qt::WindowMaximizeButtonHint);
  winner_window = new Winner();
  welcome_window = new welcome();
  welcome_window->show();
  player_lost = new PlayerLost();

  connect(this->winner_window, SIGNAL(isClosing()), this->main_game_window,
          SLOT(close()));
  connect(this->welcome_window, &welcome::setupClient, this,
          [this]() { this->client = new Client(); });
}

void Game::start(std::string path, std::vector<int> cities) {
  Shop::setArmyPrice(1, 2, 4, 3);

  map = new Map(path);

  this->initializePlayersAndResources(numOfPlayers, cities);

  if (getWelcomeWindow()->getNetwork()) {
    for (auto& m : this->map->getHexboard()->fields) {
      for (auto n : m) {
        if (this->getCurrentPlayer()->getName() != this->client->getPlayer()) {
          n->getHex()->setEnabled(false);
        }
      }
    }
  }
}

Qt::GlobalColor Game::getColor(int i) {
  std::vector<Qt::GlobalColor> colors = {
      Qt::GlobalColor::darkRed, Qt::GlobalColor::black,
      Qt::GlobalColor::darkBlue, Qt::GlobalColor::darkMagenta};
  return colors[i];
}

void Game::initializePlayersAndResources(int numOfPlayers,
                                         std::vector<int> indexCities) {
  int money = 5;

  // Initialization of players
  for (int i = 1; i <= numOfPlayers; i++) {
    std::string name = "Player" + std::to_string(i);

    Player* player = new Player(name, money, getColor(i - 1));
    players.push_back(player);
  }

  // Assigning army and cities to players
  std::vector<Field*> cities = map->getCities();

  if (!getWelcomeWindow()->getNetwork()) {
    size_t numOfCities = cities.size();
    std::vector<int> indexes(numOfCities);
    std::iota(indexes.begin(), indexes.end(),
              0);  // making vector of consequtive numbers from 0 to numOfCities

    for (Player* player : players) {
      // getting unique index from set cities
      size_t size = indexes.size();
      if (size == 0)
        std::cerr << "Not enough cities for all the players" << std::endl;
      int index = QRandomGenerator::global()->bounded(static_cast<int>(size));
      // int index = 0;
      int indexOfCity = indexes[index];
      indexes.erase(indexes.begin() + index);

      Field* city = cities[indexOfCity];
      player->addCity(*city);

      // TODO check these pointers
      Tank* tank =
          new Tank(player->getName());  // TODO place army to available fields
      city->setArmy(tank);
      player->addArmy(*tank);
    }
  } else {
    int i = 0;
    for (Player* player : players) {
      Field* city = cities[indexCities[i]];
      i++;
      player->addCity(*city);

      // TODO check these pointers
      Tank* tank =
          new Tank(player->getName());  // TODO place army to available fields
      city->setArmy(tank);
      player->addArmy(*tank);
    }
  }

  initializeTurn(*players[0]);
}

void Game::declareWinner(Player& player) {
  std::string text = "The winner is " + player.getName() + "!";
  QString inText = QString::fromStdString(text);
  winner_window->setWinner(inText);
  winner_window->show();
}

Game::~Game() {
  delete welcome_window;
  delete main_game_window;
  delete winner_window;
  delete player_lost;
  delete map;
}

void Game::addMoneyForCities(Player& player) {
  int moneyPerCity = 2;
  int numOfCities = player.getCities().size();
  player.addMoney(moneyPerCity * numOfCities);
}

void Game::initializeTurn(Player& player) {
  player.setTurn(true);

  for (auto& army : player.getArmy())  // Resetting attack and move to false
    army->resetAttAndMoveStats();

  addMoneyForCities(player);  // Adding money for every owned city
  main_game_window->setPlayer(player);
}

void Game::checkPlayers() {
  bool someone_lost = false;
  for (Player* player : this->players)  // Deleting players without an army
    if (player->getArmy().size() == 0) {
      if (player->getTurn()) {  // If player without army is on turn, set turn
                                // to next player
        Player* nextPlayer = this->getNextPlayer();
        nextPlayer->setTurn(true);
        this->initializeTurn(*nextPlayer);
      }
      std::string text = player->getName() + " lost";
      QString inText = QString::fromStdString(text);
      player_lost->setLabel(inText);
      someone_lost = true;
      this->deletePlayer(*player);
    }

  if (this->players.size() ==
      1) {  // If only one player is still alive, declare him a winner
    this->declareWinner(*players[0]);
  } else if (someone_lost) {
    player_lost->show();
  }
}

Player* Game::getCurrentPlayer() {
  for (auto player : players)
    if (player->getTurn()) {
      return player;
    }
  return nullptr;
}

Player* Game::getPlayerByName(std::string name) {
  for (auto player : players)
    if (name == player->getName())
      return player;

  return nullptr;
}

void Game::deletePlayer(Player& player) {
  for (int i = 0; i < static_cast<int>(this->players.size()); i++)
    if (&player == players[i]) {
      this->players.erase(players.begin() + i);
    }
}

void Game::setNextPlayer() {
  int index = 0;
  Player* currentPlayer = this->getCurrentPlayer();
  while (currentPlayer != players[index])
    index++;

  if (index == static_cast<int>(this->players.size()) - 1) {
    index = 0;
  } else
    index++;

  currentPlayer->setTurn(false);
  this->checkPlayers();
  this->initializeTurn(*this->players[index]);
}

Player* Game::getNextPlayer() {
  int index = 0;
  Player* currentPlayer = this->getCurrentPlayer();
  while (currentPlayer != players[index])
    index++;

  if (index == this->numOfPlayers - 1) {
    index = 0;
  } else
    index++;
  return this->players[index];
}

void Game::setNumberOfPlayers(int num) {
  this->numOfPlayers = num;
}

void Game::updateShop(Field* f) {
  TheArmy* army = main_game_window->getArmy();
  Player* player = this->getCurrentPlayer();
  f->setArmy(army);
  f->setOccupied(true);
  f->setOwner(player->getName());
  player->subtractMoney(*army);

  std::set<Field*> cities = this->getCurrentPlayer()->getCities();
  for (auto city : cities) {
    for (auto tmp : this->map->getNeighbors(city)) {
      tmp->getHex()->setState(FieldState::None);
    }
  }

  main_game_window->resetWindow();
}

void Game::showAvailableFields(Field* f) {
  f->getHex()->setState(FieldState::Selected);
  this->state->resetFields();

  if (f->getArmy()->getMaxMoveDistance())
    this->state->legalMoves =
        this->map->getLegalMoveFields(f, f->getArmy()->getMaxMoveDistance());
  if (f->getArmy()->getAttackReach())
    this->state->legalAttacks =
        this->map->getLegalAttackFields(f, f->getArmy()->getAttackReach());

  this->state->selectField = f;

  for (auto tmp : this->state->legalMoves)
    tmp->getHex()->setState(FieldState::Move);

  for (auto tmp : this->state->legalAttacks)
    tmp->getHex()->setState(FieldState::Attack);
}

void Game::MakeMoveOrAttack(Field* f, double a, double b, double c, double d) {
  // checking if next selected field is reachable for moving
  if (std::find(this->state->legalMoves.begin(), this->state->legalMoves.end(),
                f) != this->state->legalMoves.end()) {
    this->map->move(*state->selectField,
                    *f);  // currentPlayer move from selectField to f
  }
  // checking if next select field is reachable for attacking
  if (std::find(this->state->legalAttacks.begin(),
                this->state->legalAttacks.end(),
                f) != this->state->legalAttacks.end()) {
    // checking if the defender can reach the attacker
    bool canFightBack = this->map->distance(this->state->selectField, f) <=
                        f->getArmy()->getAttackReach();
    int result;
    if (getWelcomeWindow()->getNetwork())
      result = this->state->selectField->getArmy()->attackNetwork(
          *f->getArmy(), canFightBack, a, b, c, d);
    else
      result = this->state->selectField->getArmy()->attack(*f->getArmy(),
                                                           canFightBack);
    if (result == 1) {  // defender died
      this->getPlayerByName(f->getOwner())->removeArmy(*f->getArmy());
      f->deinitField();
      this->checkPlayers();
    } else if (result == 2) {  // attacker died
      this->getPlayerByName(this->state->selectField->getOwner())
          ->removeArmy(*this->state->selectField->getArmy());
      this->state->selectField->deinitField();
      this->checkPlayers();
    }
  }
  this->state->resetFields();
}

MainGameWindow* Game::getMainGameWindow() {
  return this->main_game_window;
}

welcome* Game::getWelcomeWindow() {
  return this->welcome_window;
}

int Game::getNumberOfPlayers() {
  return this->numOfPlayers;
}

Client* Game::getClient() {
  return this->client;
}

Winner* Game::getWinnerWindow() {
  return this->winner_window;
}
