#include "../headers/Cannon.h"
#include "../headers/Shop.h"

Cannon::Cannon(std::string owner) {
  this->_owner = owner;
  this->_attackStrength = 4;
  this->_maxMoveDistance = 3;
  this->_health = 10;
  this->_attackReach = 2;
  this->_price = Shop::getArmyPrice(ArmyType(cannon));
}

Cannon::~Cannon() {}

void Cannon::specialAbility(Hex* hex) {
  if (hex->getType() == FieldType::Mountain)
    this->setAttackReach(3);
  else
    this->setAttackReach(2);
  if (hex->getType() == FieldType::Clear)
    this->setAttackStrength(2);
  else
    this->setAttackStrength(4);
}
