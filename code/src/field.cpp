#include "../headers/field.h"
#include <QPainter>
#include "../headers/Game.h"
#include "../headers/State.h"

extern Game* game;

Field::~Field() {
  delete hex;
  if (this->isOccupied())
    delete _placedArmy;
}

Field::Field(const Field& other)
    : _x(other._x), _y(other._y), _type(other._type) {
  hex = new Hex(_x, _y, _type);
}

Field& Field::operator=(const Field& other) {
  auto tmp(other);

  std::swap(tmp._x, _x);
  std::swap(tmp._y, _y);
  std::swap(tmp._type, _type);

  return *this;
}

Field::Field(int x, int y, FieldType type) : _x(x), _y(y), _type(type) {
  hex = new Hex(_x, _y, _type);
  // this->getHex()->setType(type, x, y);
}

bool Field::isOccupied() const {
  return _occupied;
}

FieldType Field::getType() const {
  return _type;
}

std::string Field::getOwner() const {
  return _owner;
}

void Field::setOccupied(bool occupied) {
  this->_occupied = occupied;
}

void Field::setOwner(std::string newOwner) {
  this->_owner = newOwner;
  if (!isOccupied())
    setOccupied(true);
}

void Field::deinitField() {
  if (this->getType() == FieldType::City && this->getOwner() != "") {
    game->getPlayerByName(this->getOwner())->removeCity(*this);
  }
  this->setOwner("");
  this->setOccupied(false);
  this->_placedArmy = nullptr;
}

void Field::initField(std::string owner, TheArmy* army) {
  this->setOccupied(true);
  this->setOwner(owner);
  this->setArmy(army);
  if (this->getType() == FieldType::City)
    game->getPlayerByName(this->getOwner())->addCity(*this);
  this->getArmy()->specialAbility(this->getHex());
}

void Field::setArmy(TheArmy* army) {
  this->_placedArmy = army;
  army->specialAbility(this->getHex());  // checking if the army should change
                                         // specs based on field
}

TheArmy* Field::getArmy() {
  return this->_placedArmy;
}

int Field::getX() const {
  return _x;
}

int Field::getY() const {
  return _y;
}

bool operator==(const Field& f1, const Field& f2) {
  return f1.getX() == f2.getX() && f1.getY() == f2.getY() &&
         f1.getType() == f2.getType();
}
