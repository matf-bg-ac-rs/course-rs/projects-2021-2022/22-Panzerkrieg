#include "../headers/Hex.h"
#include <QLabel>
#include <QObject>
#include <iostream>
#include <sstream>
#include <vector>
#include "../headers/Cannon.h"
#include "../headers/Cavalry.h"
#include "../headers/Game.h"
#include "../headers/Infantry.h"
#include "../headers/Tank.h"
extern Game* game;

Hex::Hex(int x, int y, FieldType type, QGraphicsItem* parent) {
  Q_UNUSED(parent)

  _hexType = type;
  this->_x = x;
  this->_y = y;

  // draw the polygon

  // points needed to fraw hexagon: (1,0), (2,0), (3,1), (2,2), (1,2), (0,1)
  QVector<QPointF> hexPoints = {_points[0], _points[1], _points[2],
                                _points[3], _points[4], _points[5]};

  // create a polygon with the scaled points
  QPolygonF hexagon(hexPoints);

  // draw the polygon
  setPolygon(hexagon);

  // connect(this, &Hex::signalClickedSomething, main_game_window,
  // &MainGameWindow::onClickedField);
}

int Hex::getScale() {
  return _scale;
}

FieldType Hex::getType() {
  return this->_hexType;
}

QRectF Hex::boundingRect() const {
  return QRectF(0, 0, 90, 60);
}

void Hex::drawImage(QPainter* painter) {
  painter->setCompositionMode(painter->CompositionMode_Darken);

  // TODO - find better images
  QImage image;
  if (_hexType == FieldType::Forest)
    image = QImage(QString("../Resources/forest.png"));
  else if (_hexType == FieldType::Mountain)
    image = QImage(QString("../Resources/mountain.png"));
  else if (_hexType == FieldType::Clear)
    image = QImage(QString("../Resources/clear22.png"));
  else if (_hexType == FieldType::City)
    image = QImage(QString("../Resources/city2.png"));

  painter->setOpacity(0.7);
  painter->drawImage(QRectF(0, 0, 90, 60), image);
  painter->setOpacity(1);
}

void Hex::drawFrame(QPainter* painter) {
  QPen pen(Qt::black);
  if (_state == FieldState::Attack)
    pen.setColor(Qt::red);
  else if (_state == FieldState::Move)
    pen.setColor(Qt::yellow);
  else if (_state == FieldState::Selected)
    pen.setColor(Qt::darkCyan);
  else if (_state == FieldState::SelectedShop)
    pen.setColor(Qt::darkMagenta);

  pen.setWidth(5);
  painter->setPen(pen);
  painter->drawConvexPolygon(this->_points, 6);
}

void Hex::drawArmy(QPainter* painter) {
  Field* field = game->map->getFieldFromCoord(this->_x, this->_y);
  bool isOcc = field->isOccupied();

  if (isOcc) {
    TheArmy* armyOnField = field->getArmy();
    int health = armyOnField->getHealth();
    std::string stringHealth = std::to_string(health);
    QString qHealth = QString::fromStdString(stringHealth);

    Qt::GlobalColor color = Qt::GlobalColor::black;

    for (Player* player : game->players) {
      if (player->getName() == armyOnField->getOwner())
        color = player->getColor();
    }

    QImage image;
    if (dynamic_cast<Tank*>(armyOnField)) {
      image = QImage(QString("../Resources/tank.png"));  // TODO - change images
      painter->setPen(color);
      painter->drawText(QRectF(40, 40, 100, 30), Qt::AlignBaseline, qHealth);
    } else if (dynamic_cast<Cannon*>(armyOnField)) {
      image = QImage(QString("../Resources/cannon.png"));
      painter->setPen(color);
      painter->drawText(QRectF(50, 37, 100, 30), Qt::AlignBaseline, qHealth);
    } else if (dynamic_cast<Cavalry*>(armyOnField)) {
      image = QImage(QString("../Resources/cavalry2.png"));
      painter->setPen(color);
      painter->drawText(QRectF(40, 45, 100, 30), Qt::AlignBaseline, qHealth);
    } else if (dynamic_cast<Infantry*>(armyOnField)) {
      image = QImage(QString("../Resources/infaantry.png"));
      painter->setPen(color);
      painter->drawText(QRectF(53, 25, 100, 30), Qt::AlignBaseline, qHealth);
    }

    painter->setCompositionMode(painter->CompositionMode_SourceOver);
    painter->drawImage(QRect(0, 0, 90, 60), image);
  }
}

void Hex::paint(QPainter* painter,
                const QStyleOptionGraphicsItem* option,
                QWidget* widget) {
  Q_UNUSED(option)
  Q_UNUSED(widget)

  this->drawImage(painter);
  this->drawFrame(painter);
  this->drawArmy(painter);
}

Hex* Field::getHex() {
  return hex;
}

void Hex::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) {
  Q_UNUSED(mouseEvent)

  // Getting clicked hex field
  if (!game->getWelcomeWindow()->getNetwork())
    placeArmy(_x, _y, 0, 0, 0, 0);
  else
    game->getClient()->hexClicked("mapPressed " + std::to_string(_x) + " " +
                                  std::to_string(_y));
}

void Hex::placeArmy(int x, int y, double a, double b, double c, double d) {
  Field* f = game->map->getFieldFromCoord(x, y);
  Player* current = game->getCurrentPlayer();

  if (_state == FieldState::SelectedShop && !f->isOccupied())
    game->updateShop(f);
  else {
    // if selected field belongs to current player, show available fields for
    // moving and attacking

    if (current->getName() == f->getOwner())
      game->showAvailableFields(f);

    else if (game->state->selectField != nullptr) {
      game->MakeMoveOrAttack(f, a, b, c, d);
    }
  }

  game->getMainGameWindow()->draw();
}

void Hex::setState(FieldState state) {
  this->_state = state;
}
