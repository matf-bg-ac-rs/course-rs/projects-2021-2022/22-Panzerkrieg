#include "../headers/Game.h"
#include "../headers/Player.h"
#include "../headers/field.h"
#include "ui_main_game_window.h"

extern Game* game;

MainGameWindow::MainGameWindow(QWidget* parent)
    : QWidget(parent), ui(new Ui::MainGameWindow) {
  ui->setupUi(this);

  this->scene = new QGraphicsScene();
  this->scene->setSceneRect(0, 0, 2048, 2048);
  QRectF rect = scene->itemsBoundingRect();
  scene->setSceneRect(rect);

  scene->setBackgroundBrush(QColor(127, 85, 3, 120));

  ui->graphicsView->setScene(this->scene);

  exit_window = new Exit();
  shop_window = new shopwindow();

  connect(this->exit_window, SIGNAL(isClosing()), this, SLOT(close()));
}

MainGameWindow::~MainGameWindow() {
  delete ui;
  delete scene;
  delete exit_window;
  delete shop_window;
}

void MainGameWindow::draw() {
  this->scene->update();
}

void MainGameWindow::setPlayer(Player& p) {
  std::string text = p.getName() + "'s turn";
  QString inText = QString::fromStdString(text);
  this->ui->label_3->setText(inText);

  std::string money = std::to_string(p.getMoney());
  text = "Available money: " + money;
  inText = QString::fromStdString(text);
  this->ui->label_6->setText(inText);
}

void MainGameWindow::placeArmy(std::string text, TheArmy* army) {
  QString inText = QString::fromStdString(text);
  this->ui->label_7->setText(inText);

  std::set<Field*> cities = game->getCurrentPlayer()->getCities();
  for (auto city : cities) {
    for (auto tmp : game->map->getNeighbors(city)) {
      tmp->getHex()->setState(FieldState::SelectedShop);
    }
  }

  this->army = army;

  this->scene->update();
}

void MainGameWindow::resetWindow() {
  QString inText = QString::fromStdString(" ");
  this->ui->label_7->setText(inText);

  std::string money = std::to_string(game->getCurrentPlayer()->getMoney());
  std::string text = "Available money: " + money;
  inText = QString::fromStdString(text);
  this->ui->label_6->setText(inText);
}

TheArmy* MainGameWindow::getArmy() {
  return this->army;
}

void MainGameWindow::on_end_turn_clicked() {
  if (!game->getWelcomeWindow()->getNetwork())
    endTurn();
}

void MainGameWindow::endTurn() {
  game->setNextPlayer();
  game->state->resetFields();
  if (game->getWelcomeWindow()->getNetwork()) {
    if (game->getClient()->getPlayer() != game->getCurrentPlayer()->getName()) {
      this->ui->shop->setEnabled(false);
      this->ui->exit->setEnabled(false);
      this->ui->end_turn->setEnabled(false);
    } else {
      this->ui->shop->setEnabled(true);
      this->ui->exit->setEnabled(true);
      this->ui->end_turn->setEnabled(true);
    }
    for (auto& m : game->map->getHexboard()->fields) {
      for (auto n : m) {
        if (game->getCurrentPlayer()->getName() !=
            game->getClient()->getPlayer())
          n->getHex()->setEnabled(false);
        else
          n->getHex()->setEnabled(true);
      }
    }
  }
}

void MainGameWindow::on_shop_clicked() {
  shop_window->setupWindow();
  shop_window->show();
}

void MainGameWindow::on_exit_clicked() {
  exit_window->show();
}

void MainGameWindow::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/background.jpg");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}

Exit* MainGameWindow::getExitWindow() {
  return this->exit_window;
}

Ui::MainGameWindow* MainGameWindow::getUI() {
  return this->ui;
}

shopwindow* MainGameWindow::getShop() {
  return this->shop_window;
}
