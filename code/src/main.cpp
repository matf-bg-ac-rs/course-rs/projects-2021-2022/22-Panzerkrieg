#include <QApplication>
#include "../headers/Game.h"
#include "../headers/client.h"

Game* game;

int main(int argc, char* argv[]) {
  QApplication a(argc, argv);
  a.setQuitOnLastWindowClosed(true);

  game = new Game();

  return a.exec();

  delete game;
}
