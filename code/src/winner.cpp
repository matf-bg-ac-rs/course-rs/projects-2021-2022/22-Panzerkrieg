#include "../headers/winner.h"
#include "ui_winner.h"

Winner::Winner(QWidget* parent) : QWidget(parent), ui(new Ui::Winner) {
  ui->setupUi(this);
}

Winner::~Winner() {
  delete ui;
}

void Winner::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/welcome_window.png");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}

void Winner::setWinner(const QString text) {
  ui->label->setText(text);
}

void Winner::on_end_game_clicked() {
  this->close();
  emit isClosing();
}

void Winner::endGameNetwork(const QString text) {
  ui->label->setText(text);
  this->show();
}

void Winner::closeEvent(QCloseEvent* event) {
  emit isClosing();
}
