#include "../headers/Cavalry.h"
#include "../headers/Shop.h"

Cavalry::Cavalry(std::string owner) {
  this->_owner = owner;
  this->_attackStrength = 2;
  this->_maxMoveDistance = 4;
  this->_health = 10;
  this->_attackReach = 1;
  this->_price = Shop::getArmyPrice(
      ArmyType(cavalry));  // maybe move enum to TheArmy class
}

Cavalry::~Cavalry() {}

void Cavalry::specialAbility(Hex* hex) {
  if (hex->getType() == FieldType::Forest)
    this->setMaxMoveDistance(3);
  else
    this->setMaxMoveDistance(4);
  if (hex->getType() == FieldType::Clear)
    this->setAttackStrength(3);
  else
    this->setAttackStrength(2);
}
