#include "../headers/Infantry.h"
#include "../headers/Shop.h"
#include "../headers/TheArmy.h"

Infantry::Infantry(std::string owner) {
  this->_owner = owner;
  this->_attackStrength = 1;
  this->_maxMoveDistance = 2;
  this->_health = 10;
  this->_attackReach = 1;
  this->_price = Shop::getArmyPrice(
      ArmyType(infantry));  // maybe move enum to TheArmy class
}

Infantry::~Infantry() {}

void Infantry::specialAbility(Hex* hex) {
  if (hex->getType() == FieldType::Forest)
    this->setAttackStrength(3);
  else
    this->setAttackStrength(1);
  if (hex->getType() == FieldType::Clear)
    this->setMaxMoveDistance(3);
  else
    this->setMaxMoveDistance(2);
}
