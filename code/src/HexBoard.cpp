#include "../headers/HexBoard.h"
#include "../headers/Game.h"

extern Game* game;

HexBoard::HexBoard() {}

HexBoard::~HexBoard() {
  for (auto& hexes : fields)
    for (auto hex : hexes)
      delete hex;
}

void HexBoard::placeHexes(int x,
                          int y,
                          int cols,
                          int rows,
                          std::vector<std::vector<FieldType>>& types) {
  int y_index = 0;
  int x_off = 0;
  for (size_t i = 0, n = cols; i < n; i++) {
    if (i % 2 == 0) {
      x_off = 0;
    } else {
      x_off = 1;
    }
    y_index = i;
    createHexColumn(x, y, rows, y_index, x_off, types[i]);
  }
}

void HexBoard::addCity(Field* field) {
  this->cities.push_back(field);
}

std::vector<Field*> HexBoard::getCities() {
  return this->cities;
}

void HexBoard::createHexColumn(int x,
                               int y,
                               int numOfRows,
                               int y_index,
                               int x_off,
                               std::vector<FieldType>& types) {
  // creates a column of Hexes at the specified location with
  // the specified number of rows
  for (size_t i = 0, n = numOfRows; i < n; i++) {
    Field* field = new Field(i, y_index, types[i]);
    if (field->getType() == FieldType::City)
      this->addCity(field);
    // TODO change hardcoding
    int size = field->getHex()->getScale();

    int x_shift = x + 2 * size * y_index;
    int y_shift = y + size * x_off;
    field->getHex()->setPos(x_shift, y_shift + 2 * size * i);

    fields[i][y_index] = field;
    game->getMainGameWindow()->scene->addItem(field->getHex());
  }
}
