#include "../headers/State.h"

void State::resetFields() {
  for (auto f : this->legalAttacks)
    f->getHex()->setState(FieldState::None);
  for (auto f : this->legalMoves)
    f->getHex()->setState(FieldState::None);

  if (selectField) {
    selectField->getHex()->setState(FieldState::None);
    selectField = nullptr;
  }
}
