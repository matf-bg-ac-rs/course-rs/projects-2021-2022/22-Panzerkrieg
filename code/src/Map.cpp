#include "../headers/Map.h"

Map::Map(std::string path) {
  this->_path = path;
  this->hexboard = new HexBoard();

  this->draw();
}

Map::~Map() {
  delete hexboard;
}

std::vector<Field*> Map::getCities() {
  return this->hexboard->getCities();
}

void Map::draw() {
  std::vector<std::vector<FieldType>> types;
  this->readMapFromFile(types);

  hexboard->fields.resize(this->rows_size,
                          std::vector<Field*>(this->cols_size));
  hexboard->placeHexes(50, 50, this->cols_size, this->rows_size, types);
}

void Map::readMapFromFile(std::vector<std::vector<FieldType>>& types) {
  std::ifstream _file(_path, std::ifstream::in);
  if (!_file.is_open())
    std::cerr << _path << " failed to open\n";

  char c;
  int n, m;
  _file >> n >> m;
  this->cols_size = m;
  this->rows_size = n;

  _file.get(c);

  types.resize(m, std::vector<FieldType>(n));
  for (int j = 0; j < n; j++) {
    for (int i = 0; i < m; i++) {
      _file.get(c);
      types[i][j] = FieldType(c);
    }
    _file.get(c);  // '\n'
  }

  _file.close();
}

std::vector<Field*> Map::getNeighbors(Field* f) {
  int x = f->getX();
  int y = f->getY();

  std::vector<std::pair<int, int>> a(6);
  std::fill(a.begin(), a.end(), std::make_pair(x, y));
  std::vector<std::pair<int, int>> b(6);

  if (y % 2 == 1)
    b = {std::make_pair(0, -1), std::make_pair(1, -1), std::make_pair(1, 0),
         std::make_pair(1, 1),  std::make_pair(0, 1),  std::make_pair(-1, 0)};
  else
    b = {std::make_pair(-1, -1), std::make_pair(0, -1), std::make_pair(1, 0),
         std::make_pair(0, 1),   std::make_pair(-1, 1), std::make_pair(-1, 0)};

  std::transform(a.begin(), a.end(), b.begin(), a.begin(),
                 [](std::pair<int, int> value1, std::pair<int, int> value2) {
                   return std::make_pair(value1.first + value2.first,
                                         value1.second + value2.second);
                 });

  std::vector<Field*> neighbors;
  for (auto p : a)
    if (p.first >= 0 && p.second >= 0 && p.first < this->rows_size &&
        p.second < this->cols_size)
      neighbors.push_back(hexboard->fields[p.first][p.second]);

  return neighbors;
}

std::vector<Field*> Map::getNeighbors(int x, int y) {
  return getNeighbors(this->hexboard->fields[x][y]);
}

Field* Map::getFieldFromCoord(int x, int y) {
  if (x < 0 || y < 0 || y >= this->cols_size || x >= this->rows_size)
    return nullptr;

  return hexboard->fields[x][y];
}

int Map::distance(Field* f1, Field* f2) {
  int x1 = f1->getX(), y1 = f1->getY();
  int x2 = f2->getX(), y2 = f2->getY();

  if (x1 == x2)
    return abs(y1 - y2);
  if (y1 == y2)
    return abs(x1 - x2);

  if (y1 > y2) {
    std::swap(y1, y2);
    std::swap(x1, x2);
  }
  int left_x, right_x;
  if (y1 % 2 == 1 && y2 % 2 == 0) {
    left_x = x1 - ((y2 - y1 + 1) / 2 - 1);
    right_x = x1 + (y2 - y1 + 1) / 2;
  } else if (y1 % 2 == 0 && y2 % 2 == 1) {
    left_x = x1 - (y2 - y1 + 1) / 2;
    right_x = x1 + ((y2 - y1 + 1) / 2 - 1);
  } else {
    left_x = x1 - (y2 - y1 + 1) / 2;
    right_x = x1 + (y2 - y1 + 1) / 2;
  }
  if (x2 >= left_x && x2 <= right_x)
    return y2 - y1;
  else {
    int min_1 = std::abs(left_x - x2);
    int min_2 = std::abs(right_x - x2);
    return y2 - y1 + std::min(min_1, min_2);
  }
}

std::vector<Field*> Map::getLegalMoveFields(Field* f, int dist) {
  return BFS(f, dist, [](Field* f2) {
    return !f2->isOccupied(); /* && f2->getType() != FieldType::City;*/
  });
}

std::vector<Field*> Map::getLegalAttackFields(Field* f, int dist) {
  std::vector<Field*> tmp =
      BFS(f, dist, [](Field* f2) { return true; });  // f2->isOccupied();});
  std::vector<Field*> result;

  for (auto f2 : tmp)
    if (f2->isOccupied() && f2->getOwner() != f->getOwner())
      result.push_back(f2);

  return result;
}

void Map::move(Field& field1, Field& field2) {
  if (field2.isOccupied() || field1.getArmy()->wasMoved())
    return;
  field2.initField(field1.getOwner(), field1.getArmy());
  field1.deinitField();
  field2.getArmy()->setMoved(true);
}

std::vector<Field*> Map::BFS(Field* v, int dist, bool (*cond)(Field*)) {
  std::vector<Field*> result;

  std::queue<Field*> q;
  q.push(v);
  std::set<Field*> visited;

  while (!q.empty()) {
    Field* s = q.front();
    q.pop();

    if (distance(v, s) > dist)
      break;

    if (s != v)
      result.push_back(s);

    for (auto neighbor : getNeighbors(s)) {
      if (visited.find(neighbor) == visited.end() && cond(neighbor)) {
        q.push(neighbor);
        visited.insert(neighbor);
      }
    }
  }
  return result;
}

HexBoard* Map::getHexboard() {
  return this->hexboard;
}
