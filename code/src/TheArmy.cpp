#include "../headers/TheArmy.h"
#include <QRandomGenerator>
#include "../headers/Game.h"
#include "../headers/Map.h"

extern Game* game;

int TheArmy::getPrice() const {
  return this->_price;
}

int TheArmy::getAttackReach() const {
  return this->_attackReach;
}

int TheArmy::getMaxMoveDistance() const {
  return this->_maxMoveDistance;
}

bool TheArmy::wasMoved() const {
  return this->_moved;
}

bool TheArmy::wasAttacking() const {
  return this->_attacked;
}

std::string TheArmy::getOwner() const {
  return this->_owner;
}

int TheArmy::getAttack() const {
  return this->_attackStrength;
}

int TheArmy::getHealth() const {
  return this->_health;
}

void TheArmy::setHealth(const int health) {
  this->_health = health;
}

void TheArmy::setOwner(std::string owner) {
  this->_owner = owner;
}

void TheArmy::setAttackStrength(int strength) {
  this->_attackStrength = strength;
}

void TheArmy::setMaxMoveDistance(int distance) {
  this->_maxMoveDistance = distance;
}

void TheArmy::setAttackReach(int reach) {
  this->_attackReach = reach;
}

int TheArmy::attack(TheArmy& defArmy, bool canFightBack) {
  // Minimum - cannon, 1health = 0.4   Maximum - Tank, 10healths = 5
  if (wasAttacking())  // Same unit cannot attack twice
    return 0;

  // Second time returning 0
  float attStrength = this->getAttack() *
                      (sqrt(static_cast<double>(this->getHealth()) / 10)) *
                      (QRandomGenerator::global()->generateDouble() * 0.4 +
                       0.6);  // multiplied by 0.6 - 1.0
  float defStrength = defArmy.getAttack() *
                      (sqrt(static_cast<double>(defArmy.getHealth()) / 10)) *
                      (QRandomGenerator::global()->generateDouble() * 0.5 +
                       0.4);  // multiplied by 0.5 - 1.0

  int attToDefDmg =
      ceil(attStrength * (QRandomGenerator::global()->generateDouble() * 0.7 +
                          0.5));  // multiplied by 0.5 - 1.2
  int defNewHealth = defArmy.getHealth() - attToDefDmg;
  this->setAttacked(true);
  defArmy.setHealth(defNewHealth);
  if (defArmy.getHealth() < 1) {
    // delete &defArmy; // TODO check this
    return 1;
  } else {
    if (canFightBack) {
      int DefToAttDmg = ceil(
          defStrength * (QRandomGenerator::global()->generateDouble() * 0.6 +
                         0.5));  // multiplied by 0.5 - 1.3
      int attNewHealth = this->getHealth() - DefToAttDmg;
      this->setHealth(attNewHealth);
      if (this->getHealth() < 1) {
        // delete this; // TODO check this
        return 2;
      }
    }
  }
  return 0;
}

int TheArmy::attackNetwork(TheArmy& defArmy,
                           bool canFightBack,
                           double a,
                           double b,
                           double c,
                           double d) {
  if (wasAttacking())  // Same unit cannot attack twice
    return 0;

  // Second time returning 0
  float attStrength = this->getAttack() *
                      (sqrt(static_cast<double>(this->getHealth()) / 10)) *
                      (a * 0.4 + 0.6);  // multiplied by 0.6 - 1.0
  float defStrength = defArmy.getAttack() *
                      (sqrt(static_cast<double>(defArmy.getHealth()) / 10)) *
                      (b * 0.5 + 0.4);  // multiplied by 0.5 - 1.0

  int attToDefDmg =
      ceil(attStrength * (c * 0.7 + 0.5));  // multiplied by 0.5 - 1.2
  int defNewHealth = defArmy.getHealth() - attToDefDmg;
  this->setAttacked(true);
  defArmy.setHealth(defNewHealth);
  if (defArmy.getHealth() < 1) {
    // delete &defArmy; // TODO check this
    return 1;
  } else {
    if (canFightBack) {
      int DefToAttDmg =
          ceil(defStrength * (d * 0.6 + 0.5));  // multiplied by 0.5 - 1.3
      int attNewHealth = this->getHealth() - DefToAttDmg;
      this->setHealth(attNewHealth);
      if (this->getHealth() < 1) {
        // delete this; // TODO check this
        return 2;
      }
    }
  }
  return 0;
}

void TheArmy::setMoved(bool moved) {
  this->_moved = moved;
}

void TheArmy::setAttacked(bool attacked) {
  this->_attacked = attacked;
}

void TheArmy::resetAttAndMoveStats() {
  this->setAttacked(false);
  this->setMoved(false);
}
