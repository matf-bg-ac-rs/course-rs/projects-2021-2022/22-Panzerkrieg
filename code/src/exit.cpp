#include "../headers/exit.h"
#include "ui_exit.h"

Exit::Exit(QWidget* parent) : QWidget(parent), ui(new Ui::Exit) {
  ui->setupUi(this);
}

Exit::~Exit() {
  delete ui;
}

void Exit::on_yes_clicked() {
  this->close();
  emit isClosing();
}

void Exit::on_no_clicked() {
  this->close();
}

void Exit::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/welcome_window.png");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}
