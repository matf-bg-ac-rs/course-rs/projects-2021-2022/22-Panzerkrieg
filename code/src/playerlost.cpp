#include "../headers/playerlost.h"
#include "ui_playerlost.h"

PlayerLost::PlayerLost(QWidget* parent)
    : QWidget(parent), ui(new Ui::PlayerLost) {
  ui->setupUi(this);
}

PlayerLost::~PlayerLost() {
  delete ui;
}

void PlayerLost::on_button_ok_clicked() {
  this->close();
}

void PlayerLost::setLabel(QString text) {
  this->ui->label->setText(text);
}

void PlayerLost::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/welcome_window.png");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}
