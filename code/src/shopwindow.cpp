#include "../headers/shopwindow.h"
#include "../headers/Cannon.h"
#include "../headers/Cavalry.h"
#include "../headers/Game.h"
#include "../headers/Infantry.h"
#include "../headers/Shop.h"
#include "../headers/Tank.h"
#include "ui_shopwindow.h"

extern Game* game;

shopwindow::shopwindow(QWidget* parent)
    : QWidget(parent), ui(new Ui::shopwindow) {
  ui->setupUi(this);
}

void shopwindow::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/background.jpg");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}

shopwindow::~shopwindow() {
  delete ui;
}

void shopwindow::on_infantry_clicked() {
  if (!game->getWelcomeWindow()->getNetwork())
    buyArmy("Infantry");
}

void shopwindow::on_tank_clicked() {
  if (!game->getWelcomeWindow()->getNetwork())
    buyArmy("Tank");
}

void shopwindow::on_cavalry_clicked() {
  if (!game->getWelcomeWindow()->getNetwork())
    buyArmy("Cavalry");
}

void shopwindow::on_cannon_clicked() {
  if (!game->getWelcomeWindow()->getNetwork())
    buyArmy("Cannon");
}

void shopwindow::buyArmy(std::string ArmyName) {
  Player* player = game->getCurrentPlayer();
  if (ArmyName == "Cannon") {
    TheArmy* cannon = new Cannon(player->getName());
    if (Shop::buy(player, cannon)) {
      std::string text = player->getName() +
                         " please place Cannon on one of the selected fields";
      game->getMainGameWindow()->placeArmy(text, cannon);
      this->close();
    } else {
      QString inText = QString::fromStdString("Not enough money!");
      this->ui->label_6->setText(inText);
      this->ui->label_6->setStyleSheet("QLabel { color : red; }");
    }
  } else if (ArmyName == "Infantry") {
    Infantry* infantry = new Infantry(player->getName());
    if (Shop::buy(player, infantry)) {
      std::string text = player->getName() +
                         " please place Infantry on one of the selected fields";
      game->getMainGameWindow()->placeArmy(text, infantry);
      this->close();
    } else {
      QString inText = QString::fromStdString("Not enough money!");
      this->ui->label_6->setText(inText);
      this->ui->label_6->setStyleSheet("QLabel { color : red; }");
    }
  } else if (ArmyName == "Tank") {
    TheArmy* tank = new Tank(player->getName());
    if (Shop::buy(player, tank)) {
      std::string text = player->getName() +
                         " please place Tank on one of the selected fields";
      game->getMainGameWindow()->placeArmy(text, tank);
      this->close();
    } else {
      QString inText = QString::fromStdString("Not enough money!");
      this->ui->label_6->setText(inText);
      this->ui->label_6->setStyleSheet("QLabel { color : red; }");
    }
  } else {
    TheArmy* cavalry = new Cavalry(player->getName());
    if (Shop::buy(player, cavalry)) {
      std::string text = player->getName() +
                         " please place Cavalry on one of the selected fields";
      game->getMainGameWindow()->placeArmy(text, cavalry);
      this->close();
    } else {
      QString inText = QString::fromStdString("Not enough money!");
      this->ui->label_6->setText(inText);
      this->ui->label_6->setStyleSheet("QLabel { color : red; }");
    }
  }
}

void shopwindow::on_exit_clicked() {
  QString inText = QString::fromStdString(" ");
  this->ui->label_6->setText(inText);
  this->close();
}

void shopwindow::setupWindow() {
  std::string money = std::to_string(game->getCurrentPlayer()->getMoney());
  std::string text = "Available money: " + money;
  QString inText = QString::fromStdString(text);
  this->ui->available_money->setText(inText);
}

Ui::shopwindow* shopwindow::getUI() {
  return this->ui;
}
