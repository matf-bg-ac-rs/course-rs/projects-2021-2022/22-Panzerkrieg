#include "../headers/client.h"
#include "../headers/Game.h"
#include "ui_main_game_window.h"
#include "ui_shopwindow.h"
#include "ui_welcome.h"

extern Game* game;

Client::Client() {
  this->_socket = new QTcpSocket();

  _socket->connectToHost(QHostAddress::LocalHost, 1234);

  if (!_socket->waitForConnected(5000)) {
    qDebug() << "Error: " << _socket->errorString();
  }
  connect(_socket, &QTcpSocket::readyRead, this, &Client::readyRead);
  connect(this, &Client::start, this, &Client::sendToServer);
  connect(_socket, &QTcpSocket::disconnected, this,
          []() { QCoreApplication::quit(); });

  connect(this, &Client::wrongNumPlayers, this, []() {
    game->getWelcomeWindow()->getUI()->player_warning->setText(
        QString::fromStdString("Wrong number of players!"));
  });

  connect(
      this, &Client::startGame, this,
      [this](const QString& text, const QString& path, const QString& city1,
             const QString& city2, const QString& city3, const QString& city4) {
        this->_player = "Player" + text.toStdString();
        std::vector<int> cities;
        cities.push_back(city1.toInt());
        cities.push_back(city2.toInt());
        cities.push_back(city3.toInt());
        cities.push_back(city4.toInt());
        game->getWelcomeWindow()->startGame(_player, path.toStdString(),
                                            cities);
        std::cout << "Player" + text.toStdString() << std::endl;
      });

  connect(game->getWelcomeWindow()->getUI()->start_game, &QPushButton::clicked,
          this, [this]() {
            emit start("start " + QString::fromStdString(std::to_string(
                                      game->getNumberOfPlayers())));
          });

  connect(game->getMainGameWindow()->getUI()->end_turn, &QPushButton::clicked,
          this, [this]() { emit endTurn("endTurn"); });

  connect(this, &Client::endTurn, this, &Client::sendToServer);

  connect(this, &Client::endForAll, this,
          []() { game->getMainGameWindow()->endTurn(); });

  connect(game->getMainGameWindow()->getShop()->getUI()->infantry,
          &QPushButton::clicked, this,
          [this]() { emit playerShopped("playerShopped Infantry"); });

  connect(game->getMainGameWindow()->getShop()->getUI()->cannon,
          &QPushButton::clicked, this,
          [this]() { emit playerShopped("playerShopped Cannon"); });

  connect(game->getMainGameWindow()->getShop()->getUI()->tank,
          &QPushButton::clicked, this,
          [this]() { emit playerShopped("playerShopped Tank"); });

  connect(game->getMainGameWindow()->getShop()->getUI()->cavalry,
          &QPushButton::clicked, this,
          [this]() { emit playerShopped("playerShopped Cavalry"); });

  connect(this, &Client::playerShopped, this, &Client::sendToServer);

  connect(this, &Client::mapAfterShop, this, [](std::string army) {
    game->getMainGameWindow()->getShop()->buyArmy(army);
  });

  connect(this, &Client::mapPressed, this, &Client::sendToServer);

  connect(this, &Client::changeMap, this,
          [](int x, int y, double a, double b, double c, double d) {
            for (auto& m : game->map->getHexboard()->fields) {
              for (auto n : m) {
                n->getHex()->placeArmy(x, y, a, b, c, d);
              }
            }
          });

  connect(game->getMainGameWindow()->getExitWindow(), &Exit::isClosing, this,
          [this]() {
            emit playerQuit(QString::fromStdString(
                "playerQuit " + game->getCurrentPlayer()->getName()));
          });

  connect(this, &Client::playerQuit, this, &Client::sendToServer);

  connect(this, &Client::endGame, this, [](std::string playerName) {
    game->getWinnerWindow()->endGameNetwork(
        QString::fromStdString(playerName + " quit! End of game"));
  });
}

Client::~Client() {
  delete _socket;
}

void Client::readyRead() {
  while (_socket->bytesAvailable() < qint64(sizeof(quint32))) {
    if (!_socket->waitForReadyRead()) {
      return;
    }
  }

  QDataStream stream(_socket);
  quint32 msgSize = 0;
  stream >> msgSize;

  int length = qint64(msgSize - sizeof(quint32));
  while (_socket->bytesAvailable() < length) {
    if (!_socket->waitForReadyRead()) {
      return;
    }
  }

  QByteArray buffer;
  buffer.resize(length - sizeof(quint32));
  stream.skipRawData(sizeof(quint32));
  stream.readRawData(buffer.data(), length - sizeof(quint32));

  auto mess = QString::fromUtf8(buffer);
  readMessage(mess);
}

void Client::sendToServer(const QString& text) {
  if (text.isEmpty() || _socket->state() != QAbstractSocket::ConnectedState)
    return;

  QDataStream socketStream(_socket);

  QByteArray byteArray = text.toUtf8();

  socketStream << byteArray;
}

void Client::readMessage(const QString& message) {
  auto words = message.split(' ');
  auto mes = words[0];

  if (mes == "wrongNumPlayers") {
    emit wrongNumPlayers();
  } else if (mes == "startGame") {
    emit startGame(words[1], words[2], words[3], words[4], words[5], words[6]);
  } else if (mes == "EndTurn") {
    emit endForAll();
  } else if (mes == "PlayerShopped") {
    emit mapAfterShop(words[1].toStdString());
  } else if (mes == "MapPressed") {
    emit changeMap(words[1].toInt(), words[2].toInt(), words[3].toDouble(),
                   words[4].toDouble(), words[5].toDouble(),
                   words[6].toDouble());
  } else if (mes == "PlayerQuit") {
    emit endGame(words[1].toStdString());
  }
}

std::string Client::getPlayer() {
  return this->_player;
}

void Client::hexClicked(std::string cords) {
  emit mapPressed(QString::fromStdString(cords));
}
