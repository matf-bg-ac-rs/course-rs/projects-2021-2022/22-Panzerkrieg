#include "../headers/Tank.h"
#include "../headers/Shop.h"

Tank::Tank(std::string owner) {
  this->_owner = owner;
  this->_attackStrength = 4;
  this->_maxMoveDistance = 4;
  this->_health = 10;
  this->_attackReach = 1;
  this->_price =
      Shop::getArmyPrice(ArmyType(tank));  // maybe move enum to TheArmy class
}

Tank::~Tank() {}

void Tank::specialAbility(Hex* hex) {
  if (hex->getType() == FieldType::Clear)
    this->setAttackStrength(5);
  else if (hex->getType() == FieldType::Forest)
    this->setAttackStrength(3);
  else if (hex->getType() == FieldType::City)
    this->setAttackStrength(2);
  else
    this->setAttackStrength(4);
}
