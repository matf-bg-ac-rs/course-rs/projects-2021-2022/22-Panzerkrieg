#include "../headers/welcome.h"
#include <QRandomGenerator>
#include "../headers/Game.h"
#include "ui_main_game_window.h"
#include "ui_welcome.h"

extern Game* game;

welcome::welcome(QWidget* parent) : QWidget(parent), ui(new Ui::welcome) {
  ui->setupUi(this);
}

welcome::~welcome() {
  delete ui;
}

void welcome::resizeEvent(QResizeEvent* event) {
  QWidget::resizeEvent(event);
  QPixmap bkgnd("../Resources/welcome_window.png");
  bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Window, bkgnd);
  this->setPalette(palette);
}

void welcome::on_start_game_clicked() {
  if (!(ui->radioButton->isChecked() || ui->radioButton_2->isChecked() ||
        ui->radioButton_3->isChecked())) {
    QString inText = QString::fromStdString("Please select number of players!");
    this->ui->warning->setText(inText);
    this->ui->warning->setStyleSheet("QLabel { color : red; }");
  }

  if (!play_on_network)
    startGame("", "", {});
}

void welcome::startGame(std::string player,
                        std::string path,
                        std::vector<int> cities) {
  if (!play_on_network) {
    int numOfMap = QRandomGenerator::global()->bounded(1, 4);
    path = "../code/maps/map" + std::to_string(numOfMap) + ".txt";
  }

  game->start(path, cities);
  if (play_on_network) {
    game->getMainGameWindow()->getUI()->player_id->setText(
        QString::fromStdString(player));

    if (player != game->getCurrentPlayer()->getName()) {
      game->getMainGameWindow()->getUI()->shop->setEnabled(false);
      game->getMainGameWindow()->getUI()->exit->setEnabled(false);
      game->getMainGameWindow()->getUI()->end_turn->setEnabled(false);
    } else {
      game->getMainGameWindow()->getUI()->shop->setEnabled(true);
      game->getMainGameWindow()->getUI()->exit->setEnabled(true);
      game->getMainGameWindow()->getUI()->end_turn->setEnabled(true);
    }
  }
  game->getMainGameWindow()->showMaximized();
  this->close();
};

Ui::welcome* welcome::getUI() {
  return this->ui;
}

void welcome::on_network_clicked() {
  this->ui->radioButton->setCheckable(true);
  this->ui->radioButton_2->setCheckable(true);
  this->ui->radioButton_3->setCheckable(true);
  play_on_network = true;
  this->ui->local->setCheckable(false);
  emit setupClient();
}

void welcome::on_local_clicked() {
  this->ui->radioButton->setCheckable(true);
  this->ui->radioButton_2->setCheckable(true);
  this->ui->radioButton_3->setCheckable(true);
  play_on_network = false;
  this->ui->network->setCheckable(false);
}

void welcome::on_radioButton_3_clicked() {
  game->setNumberOfPlayers(2);
}

void welcome::on_radioButton_2_clicked() {
  game->setNumberOfPlayers(3);
}

void welcome::on_radioButton_clicked() {
  game->setNumberOfPlayers(4);
}

bool welcome::getNetwork() {
  return this->play_on_network;
}
