#include <tests/catch.hpp>
#include "headers/Infantry.h"

TEST_CASE("getAttack", "[Infantry]") {
  SECTION("Getter getAttack returns 1") {
    // Arrange
    const auto expected = 1;
    std::string newOwner = "Tamara";
    Infantry infantry(newOwner);

    // Act
    const auto result = infantry.getAttack();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getMaxMoveDistance Infantry", "[Infantry]") {
  SECTION("Getter getMaxMoveDistance() returns 2") {
    // Arrange
    const auto expected = 2;
    std::string newOwner = "Tamara";
    Infantry infantry(newOwner);

    // Act
    const auto result = infantry.getMaxMoveDistance();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getHealth Infantry", "[Infantry]") {
  SECTION("Getter getHealth() returns 10") {
    // Arrange
    const auto expected = 10;
    std::string newOwner = "Tamara";
    Infantry infantry(newOwner);

    // Act
    const auto result = infantry.getHealth();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("getAttackReach Infantry", "[Infantry]") {
  SECTION("Getter getAttackReach() returns 1") {
    // Arrange
    const auto expected = 1;
    std::string newOwner = "Tamara";
    Infantry infantry(newOwner);

    // Act
    const auto result = infantry.getAttackReach();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("specialAbility Infantry", "[Infantry]") {
  SECTION("When field type is forest, sets Infantry attack strength to 3") {
    Infantry infantry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Forest);

    const int expected = 3;

    infantry.specialAbility(hex);

    int result = infantry.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is not forest, sets Infantry attack strength to 1") {
    Infantry infantry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Mountain);

    const int expected = 1;

    infantry.specialAbility(hex);

    int result = infantry.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is clear, sets Infantry max move distance to 3") {
    Infantry infantry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Clear);

    const int expected = 3;

    infantry.specialAbility(hex);

    int result = infantry.getMaxMoveDistance();

    REQUIRE(result == expected);
  }
  SECTION(
      "When field type is not clear, sets Infantry max move distance to 2") {
    Infantry infantry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Forest);

    const int expected = 2;

    infantry.specialAbility(hex);

    int result = infantry.getMaxMoveDistance();

    REQUIRE(result == expected);
  }
}
