#include <tests/catch.hpp>
#include "headers/Game.h"
#include "headers/Tank.h"

#define CATCH_CONFIG_MAIN

TEST_CASE("getColor", "[Game]") {
  SECTION("If argument is 0 -> darkRed") {
    // Arrange
    const Qt::GlobalColor expected = Qt::GlobalColor::darkRed;
    Game* game = new Game();
    // Act
    const Qt::GlobalColor out = game->getColor(0);

    // Assert
    REQUIRE(expected == out);
  }
}

TEST_CASE("initializePlayersAndResources", "[Game]") {
  SECTION("If numOfPlayers equals 2 -> vector players is dimension 2") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    const int expectedSize = 2;
    game->initializePlayersAndResources(2, {});
    // Act
    const int size = game->players.size();

    // Assert
    REQUIRE(expectedSize == size);
  }

  SECTION("Every player must have a city") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    const int expectedSize = 1;
    game->initializePlayersAndResources(3, {});
    // Act
    std::vector<int> outVector;
    for (Player* player : game->players) {
      outVector.push_back(player->getCities().size());
    }
    // Assert
    for (int i = 0; i < 3; i++) {
      CHECK(outVector[i] == expectedSize);
    }
  }

  SECTION("Every player,after initialization, must have a tenk on city field") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    // Act
    std::vector<Tank*> outVector;

    for (Player* player : game->players) {
      std::set<Field*> cities = player->getCities();
      Field* city = *(cities.begin());
      TheArmy* army = city->getArmy();
      outVector.push_back(dynamic_cast<Tank*>(army));
    }
    // Assert
    for (int i = 0; i < 3; i++) {
      CHECK(outVector[i] != nullptr);
    }
  }

  SECTION("After initialization Player1 is on turn") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    const bool expectedTurn = true;
    game->initializePlayersAndResources(3, {});
    Player* player1 = game->players[0];
    // Act
    const bool realTurn = player1->getTurn();
    // Assert
    REQUIRE(expectedTurn == realTurn);
  }
}

TEST_CASE("addMoneyToCities", "[Game]") {
  SECTION(
      "After call of function addMoneyToCities for Player1,Player1 have 9 "
      "on his money saldo") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    const int expected = 9;
    game->initializePlayersAndResources(3, {});
    Player* player1 = game->players[0];
    // Act
    game->addMoneyForCities(*player1);
    const int money = player1->getMoney();
    // Assert
    REQUIRE(money == expected);
  }
}

TEST_CASE("initializeTurn", "[Game]") {
  SECTION("If argument is Player2 -> Player2 is on turn") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    const bool expectedTurn = true;
    game->initializePlayersAndResources(3, {});
    Player* player2 = game->players[1];
    // Act
    game->initializeTurn(*player2);
    const bool realTurn = player2->getTurn();
    // Assert
    REQUIRE(expectedTurn == realTurn);
  }
}

TEST_CASE("setNextPlayer", "[Game]") {
  SECTION(
      "After initialization and call of this function,Player2 is next player") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    Player* player2 = game->players[1];

    // Act
    game->setNextPlayer();
    const bool turn = player2->getTurn();
    // Assert
    REQUIRE(turn);
  }

  SECTION(
      "After initialization and call of this function,Player1's turn is "
      "setted on false") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    Player* player1 = game->players[0];
    const bool expectedValue = false;
    // Act
    game->setNextPlayer();
    const bool turn = player1->getTurn();
    // Assert
    REQUIRE(turn == expectedValue);
  }
}

TEST_CASE("getCurrentPlayer", "[Game]") {
  SECTION("After initializatin current player is player Player1") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    Player* player1 = game->players[0];
    const Player* expectedPlayer = player1;
    // Act
    Player* currPlayer = game->getCurrentPlayer();
    // Assert
    REQUIRE(currPlayer == expectedPlayer);
  }
}

TEST_CASE("getPlayerByName", "[Game]") {
  SECTION(
      "For argument Player2,after call of this function we get player number "
      "2") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    std::string playerName = "Player2";
    const Player* expectedPlayer = game->players[1];
    // Act
    Player* player = game->getPlayerByName(playerName);
    // Assert
    REQUIRE(player == expectedPlayer);
  }

  SECTION(
      "In game with 3 players,for argument Player4,after call of this "
      "function we get nullptr") {
    // Arrange
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    std::string playerName = "Player4";
    const Player* expectedPlayer = nullptr;
    // Act
    Player* player = game->getPlayerByName(playerName);
    // Assert
    REQUIRE(player == expectedPlayer);
  }
}

TEST_CASE("deletePlayer", "[Game]") {
  SECTION(
      "After call of this function,size of vector players was reduced for 1") {
    Game* game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    game->initializePlayersAndResources(3, {});
    const int expectedSize = game->players.size() - 1;
    // Act
    game->deletePlayer(*(game->players[1]));
    const int size = game->players.size();
    // Assert
    REQUIRE(expectedSize == size);
  }
}
