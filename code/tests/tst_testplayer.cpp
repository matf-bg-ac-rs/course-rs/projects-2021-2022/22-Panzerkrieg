#include <set>
#include <stdexcept>
#include <string>
#include <tests/catch.hpp>
#include "headers/Cannon.h"
#include "headers/Cavalry.h"
#include "headers/Game.h"
#include "headers/Infantry.h"
#include "headers/Player.h"
#include "headers/Tank.h"

extern Game* game;

TEST_CASE("Player", "[class]") {
  SECTION("When called with valid arguments, then not throwing exception") {
    const auto entryName = "Player1";
    const auto entryMoney = 5;
    const auto entryColor = Qt::GlobalColor::red;

    Player p(entryName, entryMoney, entryColor);

    REQUIRE_NOTHROW(Player(entryName, entryMoney, entryColor));
  }
}

TEST_CASE("getCities", "[Player]") {
  SECTION("When player don't have any city, then returns empty set") {
    Player p("Player1", 5, Qt::GlobalColor::red);

    auto result = p.getCities();

    REQUIRE(result.size() == 0);
  }
}

TEST_CASE("addCity", "[Player]") {
  SECTION("When initialized, player don't have any city") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    const int expected = 0;

    auto result = p.getCities();

    REQUIRE(result.size() == expected);
  }

  SECTION("When player add one city, then player have that city") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f = new Field(1, 1, FieldType::City);
    const auto expectedSize = 1;

    p.addCity(*f);
    auto result = p.getCities();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(f) != result.end());
  }

  SECTION("When player add a city, then that city is set occupied") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f = new Field(1, 1, FieldType::City);
    const auto expected = true;

    p.addCity(*f);
    auto result = f->isOccupied();

    REQUIRE(result == expected);
  }

  SECTION("When player add a city, then player is owner of that city") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f = new Field(1, 1, FieldType::City);
    const auto expected = p.getName();

    p.addCity(*f);
    auto result = f->getOwner();

    REQUIRE(result == expected);
  }

  SECTION("When player add more cities, then player has that cities") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f1 = new Field(1, 1, FieldType::City);
    Field* f2 = new Field(1, 2, FieldType::City);
    Field* f3 = new Field(1, 3, FieldType::City);
    const auto expectedSize = 3;

    p.addCity(*f1);
    p.addCity(*f2);
    p.addCity(*f3);
    auto result = p.getCities();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(f1) != result.end());
    REQUIRE(result.find(f2) != result.end());
    REQUIRE(result.find(f3) != result.end());
  }
}

TEST_CASE("removeCity", "[Player]") {
  SECTION(
      "When removing only city that player own , player doesn't have that "
      "city") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f = new Field(1, 1, FieldType::City);
    const auto expectedSize = 0;

    p.addCity(*f);
    p.removeCity(*f);
    const auto result = p.getCities();

    CHECK(result.size() == expectedSize);
    REQUIRE_FALSE(result.find(f) != result.end());
  }

  SECTION("When removing one of two cities, player still have the other city") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f1 = new Field(1, 1, FieldType::City);
    Field* f2 = new Field(2, 2, FieldType::City);
    const auto expectedSize = 1;

    p.addCity(*f1);
    p.addCity(*f2);
    p.removeCity(*f1);
    const auto result = p.getCities();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(f2) != result.end());
  }

  SECTION(
      "When removing more cities , player have only remainig cities and "
      "doesn't have removed cities") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    Field* f1 = new Field(1, 1, FieldType::City);
    Field* f2 = new Field(1, 1, FieldType::City);
    Field* f3 = new Field(1, 1, FieldType::City);
    Field* f4 = new Field(1, 1, FieldType::City);
    const auto expectedSize = 2;

    p.addCity(*f1);
    p.addCity(*f2);
    p.addCity(*f3);
    p.addCity(*f4);
    p.removeCity(*f1);
    p.removeCity(*f2);

    const auto result = p.getCities();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(f3) != result.end());
    REQUIRE(result.find(f4) != result.end());
    REQUIRE_FALSE(result.find(f1) != result.end());
    REQUIRE_FALSE(result.find(f2) != result.end());
  }
}

TEST_CASE("addArmy", "[Player]") {
  SECTION("At start , player don't have any army") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    const auto expectedSize = 0;

    const auto result = p.getArmy();

    REQUIRE(result.size() == expectedSize);
  }

  SECTION("When adding one army , player have that army") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    TheArmy* tank = new Tank(p.getName());
    const auto expectedSize = 1;

    p.addArmy(*tank);
    const auto result = p.getArmy();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(tank) != result.end());
  }

  SECTION("When adding more armies , player have that armies") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    TheArmy* tank = new Tank(p.getName());
    TheArmy* cavalry = new Cavalry(p.getName());
    TheArmy* cannon = new Cannon(p.getName());
    TheArmy* infantry = new Infantry(p.getName());
    const auto expectedSize = 4;

    p.addArmy(*tank);
    p.addArmy(*cavalry);
    p.addArmy(*cannon);
    p.addArmy(*infantry);

    const auto result = p.getArmy();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(tank) != result.end());
    REQUIRE(result.find(cavalry) != result.end());
    REQUIRE(result.find(cannon) != result.end());
    REQUIRE(result.find(infantry) != result.end());
  }
}

TEST_CASE("removeArmy", "[Player]") {
  SECTION(
      "When removing only army that player own , player doesn't have that "
      "army") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    TheArmy* tank = new Tank(p.getName());
    const auto expectedSize = 0;

    p.addArmy(*tank);
    p.removeArmy(*tank);
    const auto result = p.getArmy();

    CHECK(result.size() == expectedSize);
    REQUIRE_FALSE(result.find(tank) != result.end());
  }

  SECTION("When removing one of two armies, player still have the other army") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    TheArmy* tank = new Tank(p.getName());
    TheArmy* cavalry = new Cavalry(p.getName());
    const auto expectedSize = 1;

    p.addArmy(*tank);
    p.addArmy(*cavalry);
    p.removeArmy(*tank);
    const auto result = p.getArmy();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(cavalry) != result.end());
  }

  SECTION(
      "When removing more armies , player have only remainig armies and "
      "doesn't have removed armies") {
    Player p("Player1", 5, Qt::GlobalColor::red);
    TheArmy* tank = new Tank(p.getName());
    TheArmy* cavalry = new Cavalry(p.getName());
    TheArmy* cannon = new Cannon(p.getName());
    TheArmy* infantry = new Infantry(p.getName());
    const auto expectedSize = 2;

    p.addArmy(*tank);
    p.addArmy(*cavalry);
    p.addArmy(*cannon);
    p.addArmy(*infantry);
    p.removeArmy(*tank);
    p.removeArmy(*cavalry);

    const auto result = p.getArmy();

    CHECK(result.size() == expectedSize);
    REQUIRE(result.find(cannon) != result.end());
    REQUIRE(result.find(infantry) != result.end());
    REQUIRE_FALSE(result.find(cavalry) != result.end());
    REQUIRE_FALSE(result.find(tank) != result.end());
  }
}

TEST_CASE("addMoney", "[Player]") {
  SECTION(
      "When calling with argument 0 , then returns the same amount of money") {
    const auto entry = 5;
    const auto addedMoney = 0;
    const auto expected = entry + addedMoney;
    Player p("Player1", entry, Qt::GlobalColor::red);

    p.addMoney(addedMoney);
    const auto result = p.getMoney();

    REQUIRE(result == expected);
  }

  SECTION(
      "When calling with some number , then returns the the amount of "
      "money plus that number") {
    const auto entry = 5;
    const auto addedMoney = 3;
    const auto expected = entry + addedMoney;
    Player p("Player1", entry, Qt::GlobalColor::red);

    p.addMoney(addedMoney);
    const auto result = p.getMoney();

    REQUIRE(result == expected);
  }
}

TEST_CASE("subtractMoney", "[Player]") {
  SECTION(
      "When calling with some army , then returns money subtracted for "
      "value of that army") {
    const auto entry = 5;
    Player p("Player1", entry, Qt::GlobalColor::red);
    TheArmy* subtracting = new Tank(p.getName());
    const auto expected = entry - subtracting->getPrice();

    p.subtractMoney(*subtracting);
    const auto result = p.getMoney();

    REQUIRE(result == expected);
  }
}

TEST_CASE("getTurn", "[Player]") {
  SECTION("At start, player turn is false ") {
    const auto expected = false;
    Player p("Player1", 5, Qt::GlobalColor::red);

    const auto result = p.getTurn();

    REQUIRE(result == expected);
  }

  SECTION("Returns player move ") {
    const auto expected = true;
    Player p("Player1", 5, Qt::GlobalColor::red);
    p.setTurn(expected);

    const auto result = p.getTurn();

    REQUIRE(result == expected);
  }
}
