#include <tests/catch.hpp>
#include "headers/Cannon.h"

TEST_CASE("getAttack Cannon", "[Cannon]") {
  SECTION("Getter getAttack returns 4") {
    // Arrange
    const auto expected = 4;
    std::string newOwner = "Tamara";
    Cannon cannon(newOwner);

    // Act
    const auto result = cannon.getAttack();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getMaxMoveDistance Cannon", "[Cannon]") {
  SECTION("Getter getMaxMoveDistance() returns 3") {
    // Arrange
    const auto expected = 3;
    std::string newOwner = "Tamara";
    Cannon cannon(newOwner);

    // Act
    const auto result = cannon.getMaxMoveDistance();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getHealth Cannon", "[Cannon]") {
  SECTION("Getter getHealth() returns 10") {
    // Arrange
    const auto expected = 10;
    std::string newOwner = "Tamara";
    Cannon cannon(newOwner);

    // Act
    const auto result = cannon.getHealth();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("getAttackReach Cannon", "[Cannon]") {
  SECTION("Getter getAttackReach() returns 2") {
    // Arrange
    const auto expected = 2;
    std::string newOwner = "Tamara";
    Cannon cannon(newOwner);

    // Act
    const auto result = cannon.getAttackReach();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("specialAbility Cannon", "[Cannon]") {
  SECTION("When field type is mountain, sets Cannons attack reach to 3") {
    Cannon cannon("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Mountain);

    const int expected = 3;

    cannon.specialAbility(hex);

    int result = cannon.getAttackReach();

    REQUIRE(result == expected);
  }
  SECTION("When field type is not mountain, sets Cannons attack reach to 2") {
    Cannon cannon("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Forest);

    const int expected = 2;

    cannon.specialAbility(hex);

    int result = cannon.getAttackReach();

    REQUIRE(result == expected);
  }
  SECTION("When field type is clear, sets Cannons attack strength to 2") {
    Cannon cannon("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Clear);

    const int expected = 2;

    cannon.specialAbility(hex);

    int result = cannon.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is not clear, sets Cannons attack strength to 4") {
    Cannon cannon("Tina");
    Hex* hex = new Hex(1, 1, FieldType::City);

    const int expected = 4;

    cannon.specialAbility(hex);

    int result = cannon.getAttack();

    REQUIRE(result == expected);
  }
}
