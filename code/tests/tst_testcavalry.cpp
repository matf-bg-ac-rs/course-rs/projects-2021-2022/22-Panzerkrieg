#include <tests/catch.hpp>
#include "headers/Cavalry.h"

TEST_CASE("getAttack Cavalry", "[Cavalry]") {
  SECTION("Getter getAttack returns 2") {
    // Arrange
    const auto expected = 2;
    std::string newOwner = "Tamara";
    Cavalry cavalry(newOwner);

    // Act
    const auto result = cavalry.getAttack();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getMaxMoveDistance Cavalry", "[Cavalry]") {
  SECTION("Getter getMaxMoveDistance() returns 4") {
    // Arrange
    const auto expected = 4;
    std::string newOwner = "Tamara";
    Cavalry cavalry(newOwner);

    // Act
    const auto result = cavalry.getMaxMoveDistance();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getHealth Cavalry", "[Cavalry]") {
  SECTION("Getter getHealth() returns 10") {
    // Arrange
    const auto expected = 10;
    std::string newOwner = "Tamara";
    Cavalry cavalry(newOwner);

    // Act
    const auto result = cavalry.getHealth();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("getAttackReach Cavalry", "[Cavalry]") {
  SECTION("Getter getAttackReach() returns 1") {
    // Arrange
    const auto expected = 1;
    std::string newOwner = "Tamara";
    Cavalry cavalry(newOwner);

    // Act
    const auto result = cavalry.getAttackReach();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("specialAbility Cavalry", "[Cavalry]") {
  SECTION("When field type is forest, sets Cavalry max move distance to 3") {
    Cavalry cavalry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Forest);

    const int expected = 3;

    cavalry.specialAbility(hex);

    int result = cavalry.getMaxMoveDistance();

    REQUIRE(result == expected);
  }
  SECTION(
      "When field type is not forest, sets Cavalry max move distance to 4") {
    Cavalry cavalry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Mountain);

    const int expected = 4;

    cavalry.specialAbility(hex);

    int result = cavalry.getMaxMoveDistance();

    REQUIRE(result == expected);
  }
  SECTION("When field type is clear, sets Cavalry attack strength to 3") {
    Cavalry cavalry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Clear);

    const int expected = 3;

    cavalry.specialAbility(hex);

    int result = cavalry.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is not clear, sets Cavalry attack strength to 2") {
    Cavalry cavalry("Tina");
    Hex* hex = new Hex(1, 1, FieldType::City);

    const int expected = 2;

    cavalry.specialAbility(hex);

    int result = cavalry.getAttack();

    REQUIRE(result == expected);
  }
}
