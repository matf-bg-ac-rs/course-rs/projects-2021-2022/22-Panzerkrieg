#include <set>
#include <stdexcept>
#include <string>
#include <tests/catch.hpp>
#include "headers/Cannon.h"
#include "headers/Game.h"
#include "headers/Infantry.h"
#include "headers/Player.h"
#include "headers/field.h"

#include <iostream>
#include "headers/Hex.h"

extern Game* game;

TEST_CASE("getX", "[Field]") {
  SECTION("Getter returns X") {
    // Arrange
    Field f1(0.0, 1.0, FieldType::City);
    // Act + Assert
    REQUIRE(f1.getX() == 0.0);
  }
}

TEST_CASE("getY", "[Field]") {
  SECTION("Getter returns Y") {
    // Arrange
    Field f1(1.0, 2.0, FieldType::City);
    // Act + Assert
    REQUIRE(f1.getY() == 2.0);
  }
}

TEST_CASE("getType", "[Field]") {
  SECTION("Getter returns type") {
    // Arrange
    Field f1(0.0, 1.0, FieldType::City);
    // Act + Assert
    REQUIRE(f1.getType() == FieldType::City);
  }
}

TEST_CASE("Constructor1", "[Field]") {
  SECTION(
      "When constructor Field(int x, int y, FieldType type) called with "
      "valid arguments, then not throwing exception") {
    const auto x = 0.0;
    const auto y = 0.0;
    const FieldType ft = FieldType::City;

    // Act + Assert
    REQUIRE_NOTHROW(Field(x, y, ft));
  }
}

TEST_CASE("Constructor2", "[Field]") {
  SECTION(
      "When constructor Field(const Field &other) called with valid "
      "arguments, then not throwing exception") {
    const auto x = 0.0;
    const auto y = 0.0;
    const FieldType ft = FieldType::City;

    // Act + Assert
    REQUIRE_NOTHROW(Field(x, y, ft));
  }
}

TEST_CASE("setOwner", "[Field]") {
  SECTION("Sets owner") {
    Field* field = new Field(1.0, 2.0, FieldType::City);
    const std::string newOwner = "Nikola";
    const auto expectedOwner = 0;

    // Act
    field->setOwner(newOwner);
    const auto matchingOwner = field->getOwner().compare(newOwner);

    // Assert
    REQUIRE(matchingOwner == expectedOwner);
  }
}

TEST_CASE("setOccupied", "[Field]") {
  SECTION("Sets field occupied") {
    Field* field = new Field(1.0, 2.0, FieldType::City);
    const bool expectedOccupied = true;

    // Act
    field->setOccupied(true);

    // Assert
    REQUIRE(field->isOccupied() == expectedOccupied);
  }
}

TEST_CASE("initField", "[Field]") {
  SECTION("Sets field owner") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::Mountain);
    Cannon* cannon = new Cannon("Tina");

    field->initField("Tina", cannon);

    const std::string expected = "Tina";

    std::string result = field->getOwner();

    REQUIRE(result == expected);
  }
  SECTION("Sets field as occupied") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::Forest);
    Cannon* cannon = new Cannon("Tina");

    field->initField("Tina", cannon);

    const bool expected = true;

    bool result = field->isOccupied();

    REQUIRE(result == expected);
  }
  SECTION("If field type is city, adds field to owners cities") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(2, 1, FieldType::City);
    Cannon* cannon = new Cannon("Tina");

    field->initField("Tina", cannon);

    std::set<Field*> result = player->getCities();

    REQUIRE(result.find(field) != result.end());
  }
  SECTION("Sets armys special ability") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(2, 1, FieldType::Clear);
    Infantry* infantry = new Infantry("Tina");

    field->initField("Tina", infantry);

    const int expected = 3;

    int result = infantry->getMaxMoveDistance();

    REQUIRE(result == expected);
  }
}

TEST_CASE("deinitField", "[Field]") {
  SECTION(
      "If field type is city, and has owner, remove city from owners cities") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::City);
    field->setOwner("Tina");

    player->addCity(*field);

    field->deinitField();

    std::set<Field*> result = player->getCities();

    REQUIRE_FALSE(result.find(field) != result.end());
  }
  SECTION("Sets owner to empty string") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::Mountain);
    field->setOwner("Tina");

    field->deinitField();

    const std::string expected = "";

    std::string result = field->getOwner();

    REQUIRE(result == expected);
  }
  SECTION("Sets occupied false") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::Mountain);
    field->setOccupied(true);

    field->deinitField();

    const bool expected = false;

    bool result = field->isOccupied();

    REQUIRE(result == expected);
  }
  SECTION("Sets places army to nullptr") {
    game = new Game();
    game->map = new Map("../code/maps/map2.txt");
    Player* player = new Player("Tina", 100, Qt::GlobalColor::magenta);
    game->players.push_back(player);

    Field* field = new Field(3, 2, FieldType::Mountain);
    Infantry* infantry = new Infantry("Tina");
    field->setArmy(infantry);

    field->deinitField();

    const TheArmy* expected = nullptr;

    TheArmy* result = field->getArmy();

    REQUIRE(result == expected);
  }
}
