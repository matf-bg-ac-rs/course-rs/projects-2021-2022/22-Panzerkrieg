#include <string>
#include <tests/catch.hpp>
#include "headers/Map.h"
#include "headers/Tank.h"
#include "headers/TheArmy.h"

TEST_CASE("getCities_", "[Map]") {
  SECTION("Map2 has cities on positions (5,1),(12,1),(1,10),(8,3)") {
    // Arrange
    Map* map = new Map("../code/maps/map1.txt");
    std::vector<Field*> expectedCities(4);
    expectedCities[0] = new Field(5, 1, FieldType::City);
    expectedCities[1] = new Field(12, 1, FieldType::City);
    expectedCities[2] = new Field(1, 10, FieldType::City);
    expectedCities[3] = new Field(8, 13, FieldType::City);

    // Act
    std::vector<Field*> outCities = map->getCities();
    // Assertion
    REQUIRE(outCities.size() == expectedCities.size());
    for (int i = 0; i < 4; i++)
      REQUIRE((*(expectedCities[i])) == (*(outCities[i])));
  }
}

TEST_CASE("getFieldFromCoord", "[Map]") {
  SECTION(
      "After initialization, map1 has field with coordinates (1,10) and "
      "type City") {
    // Arrange
    Map* map = new Map("../code/maps/map1.txt");
    Field* expectedField = new Field(1, 10, FieldType::City);
    // Act
    Field* outField = map->getFieldFromCoord(1, 10);
    // Assertion
    REQUIRE(*expectedField == *outField);
  }

  SECTION(
      "After initialization, map3 has field with coordinates (2,0) and "
      "type Clear") {
    // Arrange
    Map* map = new Map("../code/maps/map3.txt");
    Field* expectedField = new Field(2, 0, FieldType::Clear);
    // Act
    Field* outField = map->getFieldFromCoord(2, 0);
    // Assertion
    REQUIRE(*expectedField == *outField);
  }

  SECTION(
      "Map for invalid coordinates (-2,0) returns that the field does not "
      "exist") {
    // Arrange
    Map* map = new Map("../code/maps/map3.txt");
    Field* expectedField = nullptr;
    // Act
    Field* outField = map->getFieldFromCoord(-2, 0);
    // Assertion
    REQUIRE(expectedField == outField);
  }

  SECTION(
      "Map for invalid coordinates (13,3) returns that the field does not "
      "exist") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    Field* expectedField = nullptr;
    // Act
    Field* outField = map->getFieldFromCoord(13, 3);
    // Assertion
    REQUIRE(outField == expectedField);
  }
}

TEST_CASE("distance", "[Map]") {
  SECTION("Distance between the same fields with coordinates (3,7) is 0") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    int expectedDistance = 0;
    Field* f1 = map->getFieldFromCoord(3, 7);
    Field* f2 = map->getFieldFromCoord(3, 7);
    // Act
    int outDistance = map->distance(f1, f2);
    // Assertion
    REQUIRE(expectedDistance == outDistance);
  }

  SECTION("Distance between fields with coordinates (2,2) and (2,7) is 5") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    int expectedDistance = 7 - 2;
    Field* f1 = map->getFieldFromCoord(2, 2);
    Field* f2 = map->getFieldFromCoord(2, 7);
    // Act
    int outDistance = map->distance(f1, f2);
    // Assertion
    REQUIRE(expectedDistance == outDistance);
  }

  SECTION("Distance between fields with coordinates (2,2) and (4,7) is 5") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    int expectedDistance = 5;
    Field* f1 = map->getFieldFromCoord(2, 2);
    Field* f2 = map->getFieldFromCoord(4, 7);
    // Act
    int outDistance = map->distance(f1, f2);
    // Assertion
    REQUIRE(expectedDistance == outDistance);
  }
}

TEST_CASE("Map", "getNeighbors") {
  SECTION(
      "Neighbors of fields with coordinates (0,0) are fields on (1,0) and "
      "(0,1)") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    std::vector<Field*> expected = {map->getFieldFromCoord(1, 0),
                                    map->getFieldFromCoord(0, 1)};
    Field* f = map->getFieldFromCoord(0, 0);
    // Act
    std::vector<Field*> out = map->getNeighbors(f);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 2; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }

  SECTION(
      "Neighbors of fields with coordinates (1,2) are fields on "
      "(0,1),(1,1),(2,2),(1,3),(0,3),(0,2)") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    std::vector<Field*> expected = {
        map->getFieldFromCoord(0, 1), map->getFieldFromCoord(1, 1),
        map->getFieldFromCoord(2, 2), map->getFieldFromCoord(1, 3),
        map->getFieldFromCoord(0, 3), map->getFieldFromCoord(0, 2)};
    Field* f = map->getFieldFromCoord(1, 2);
    // Act
    std::vector<Field*> out = map->getNeighbors(f);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 6; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }

  SECTION(
      "Neighbors of fields with coordinates (1,1) are fields on (1,0), "
      "(2,0), (2,1), (2,2), (1,2), (0,1)") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    std::vector<Field*> expected = {
        map->getFieldFromCoord(1, 0), map->getFieldFromCoord(2, 0),
        map->getFieldFromCoord(2, 1), map->getFieldFromCoord(2, 2),
        map->getFieldFromCoord(1, 2), map->getFieldFromCoord(0, 1)};
    Field* f = map->getFieldFromCoord(1, 1);
    // Act
    std::vector<Field*> out = map->getNeighbors(f);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 6; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }
}

TEST_CASE("getLegalMoveFields", "[Map]") {
  SECTION(
      "The legal fields for moving the army from the field (0,0) with a "
      "maximum step of 1 are the fields with coodinates (1,0) and (0,1)") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");
    std::vector<Field*> expected = {map->getFieldFromCoord(1, 0),
                                    map->getFieldFromCoord(0, 1)};
    Field* f = map->getFieldFromCoord(0, 0);
    int dist = 1;
    // Act
    std::vector<Field*> out = map->getLegalMoveFields(f, dist);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 2; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }

  SECTION(
      "The legal fields for moving the army from the field (4,2) with a "
      "maximum "
      "step of 1 are the fields with coodinates "
      "(4,1),(5,2),(4,3),(3,3),(3,2)") {
    // Arrange
    Map* map = new Map("../code/maps/map1.txt");
    std::vector<Field*> expected = {
        // map->getFieldFromCoord(3,1),
        map->getFieldFromCoord(4, 1), map->getFieldFromCoord(5, 2),
        map->getFieldFromCoord(4, 3), map->getFieldFromCoord(3, 3),
        map->getFieldFromCoord(3, 2)};
    Field* f = map->getFieldFromCoord(4, 2);
    int dist = 1;
    Field* f1 = map->getFieldFromCoord(3, 1);
    f1->setOccupied(true);
    // Act
    std::vector<Field*> out = map->getLegalMoveFields(f, dist);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 5; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }
}

TEST_CASE("getLegalAttackFields", "[Map]") {
  SECTION("From field (1,1) there are no legal fields that can be attacked") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");

    Field* f1 = map->getFieldFromCoord(1, 1);
    f1->initField("Player1", new Tank("Player1"));

    std::vector<Field*> expected = {};

    int dist = 1;

    // Act
    std::vector<Field*> out = map->getLegalAttackFields(f1, dist);
    // Assertion
    REQUIRE(out.size() == expected.size());
  }

  SECTION(
      "If the field (1,2) has an army, it is a legal field to attack from "
      "the field (1,1) ") {
    // Arrange
    Map* map = new Map("../code/maps/map2.txt");

    Field* f1 = map->getFieldFromCoord(1, 1);
    f1->initField("Player1", new Tank("Player1"));

    Field* f2 = map->getFieldFromCoord(1, 2);
    f2->initField("Player2", new Tank("Player2"));

    std::vector<Field*> expected = {f2};

    int dist = 1;

    // Act
    std::vector<Field*> out = map->getLegalAttackFields(f1, dist);
    // Assertion
    REQUIRE(out.size() == expected.size());
    for (int i = 0; i < 1; i++)
      REQUIRE(*(expected[i]) == *(out[i]));
  }
}

TEST_CASE("move", "[Map]") {
  SECTION(
      "If field where we want to move the army is occupied, nothing is "
      "being done ") {
    // Arrange
    Map* map = new Map("../code/maps/map1.txt");

    Field* f1 = map->getFieldFromCoord(1, 3);
    f1->initField("Player1", new Tank("Player1"));

    Field* f2 = map->getFieldFromCoord(1, 4);
    f2->initField("Player2", new Tank("Player2"));

    bool expectedF1Occupied = true;
    TheArmy* expectedArmyOnF1 = f1->getArmy();
    std::string expectedPlayerF1 = f1->getOwner();

    bool expectedF2Occupied = true;
    TheArmy* expectedArmyOnF2 = f2->getArmy();
    std::string expectedPlayerF2 = f2->getOwner();

    // Act
    map->move(*f1, *f2);

    bool outF1Occupied = f1->isOccupied();
    TheArmy* outArmyOnF1 = f1->getArmy();
    std::string outPlayerF1 = f1->getOwner();

    bool outF2Occupied = f2->isOccupied();
    TheArmy* outArmyOnF2 = f2->getArmy();
    std::string outPlayerF2 = f2->getOwner();

    // Assertion
    REQUIRE(expectedF1Occupied == outF1Occupied);
    REQUIRE(expectedArmyOnF1 == outArmyOnF1);
    REQUIRE(expectedPlayerF1 == outPlayerF1);

    REQUIRE(expectedF2Occupied == outF2Occupied);
    REQUIRE(expectedArmyOnF2 == outArmyOnF2);
    REQUIRE(expectedPlayerF2 == outPlayerF2);
  }

  SECTION("The tank moves from the field (1,1) to the field (1,3)") {
    // Arrange
    Map* map = new Map("../code/maps/map1.txt");

    Field* f1 = map->getFieldFromCoord(1, 1);
    f1->initField("Player1", new Tank("Player1"));

    bool expectedF1Occupied = false;
    TheArmy* expectedArmyOnF1 = nullptr;
    std::string expectedPlayerF1 = "";

    Field* f2 = map->getFieldFromCoord(1, 3);

    bool expectedF2Occupied = true;
    TheArmy* expectedArmyOnF2 = f1->getArmy();
    std::string expectedPlayerF2 = f1->getOwner();

    // Act
    map->move(*f1, *f2);

    bool outF1Occupied = f1->isOccupied();
    TheArmy* outArmyOnF1 = f1->getArmy();
    std::string outPlayerF1 = f1->getOwner();

    bool outF2Occupied = f2->isOccupied();
    TheArmy* outArmyOnF2 = f2->getArmy();
    std::string outPlayerF2 = f2->getOwner();

    // Assertion
    CHECK(expectedF1Occupied == outF1Occupied);
    CHECK(expectedArmyOnF1 == outArmyOnF1);
    CHECK(expectedPlayerF1 == outPlayerF1);

    CHECK(expectedF2Occupied == outF2Occupied);
    CHECK(expectedArmyOnF2 == outArmyOnF2);
    CHECK(expectedPlayerF2 == outPlayerF2);
  }
}
