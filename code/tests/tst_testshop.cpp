#include <set>
#include <tests/catch.hpp>
#include "../headers/Shop.h"
#include "headers/Cannon.h"
#include "headers/Cavalry.h"
#include "headers/Game.h"
#include "headers/Infantry.h"
#include "headers/Tank.h"

extern Game* game;

TEST_CASE("setArmyPrice", "[Shop]") {
  SECTION(
      "When instantiating Army, if setArmyPrice is not called beforehand, "
      "price is zero") {
    Player* p = new Player("Player1", 5, Qt::GlobalColor::blue);
    Infantry* i = new Infantry(p->getName());
    Cavalry* c = new Cavalry(p->getName());
    Tank* t = new Tank(p->getName());
    Cannon* cn = new Cannon(p->getName());

    int result1 = i->getPrice();
    int result2 = c->getPrice();
    int result3 = t->getPrice();
    int result4 = cn->getPrice();

    REQUIRE(result1 == 0);
    REQUIRE(result2 == 0);
    REQUIRE(result3 == 0);
    REQUIRE(result4 == 0);
  }
  SECTION(
      "When instantiating Army, if setArmyPrice is called beforehand, "
      "inputed price is set") {
    Shop::setArmyPrice(15, 5, 3, 8);
    Player* p = new Player("Player1", 5, Qt::GlobalColor::blue);
    Infantry* i = new Infantry(p->getName());
    Cavalry* c = new Cavalry(p->getName());
    Tank* t = new Tank(p->getName());
    Cannon* cn = new Cannon(p->getName());

    int result1 = i->getPrice();
    int result2 = c->getPrice();
    int result3 = t->getPrice();
    int result4 = cn->getPrice();

    REQUIRE(result1 == 15);
    REQUIRE(result2 == 5);
    REQUIRE(result3 == 3);
    REQUIRE(result4 == 8);
  }
}

TEST_CASE("buy", "[Shop]") {
  SECTION(
      "When player doesn't have enough money for what he wants to buy, "
      "returns false") {
    Shop::setArmyPrice(1, 2, 4, 3);
    Player* p = new Player("Player1", 2, Qt::GlobalColor::black);
    Cannon* c = new Cannon(p->getName());

    const bool expected = false;

    bool result = Shop::buy(p, c);

    REQUIRE(result == expected);
  }
  SECTION(
      "When player has enough money for what he wants to buy, returns true") {
    Shop::setArmyPrice(1, 2, 4, 3);
    Player* p = new Player("Player1", 10, Qt::GlobalColor::black);
    Tank* c = new Tank(p->getName());

    const bool expected = true;

    bool result = Shop::buy(p, c);

    REQUIRE(result == expected);
  }
  SECTION(
      "When player has enough money for what he wants to buy, adds that "
      "army to his army") {
    Shop::setArmyPrice(1, 2, 4, 3);
    Player* p = new Player("Player1", 10, Qt::GlobalColor::black);
    Infantry* c = new Infantry(p->getName());

    Shop::buy(p, c);

    auto result = p->getArmy();

    REQUIRE(result.find(c) != result.end());
  }
  SECTION(
      "When player doesn't have enough money for what he wants to buy, "
      "doesn't add that army to his army") {
    Shop::setArmyPrice(1, 2, 4, 3);
    Player* p = new Player("Player1", 1, Qt::GlobalColor::black);
    Cavalry* c = new Cavalry(p->getName());

    Shop::buy(p, c);

    auto result = p->getArmy();

    REQUIRE_FALSE(result.find(c) != result.end());
  }
}
