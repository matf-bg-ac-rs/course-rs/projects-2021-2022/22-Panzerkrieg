#include <tests/catch.hpp>
#include "headers/Tank.h"

TEST_CASE("getAttack Tank", "[Tank]") {
  SECTION("Getter getAttack returns 4") {
    // Arrange
    const auto expected = 4;
    std::string newOwner = "Tamara";
    Tank tank(newOwner);

    // Act
    const auto result = tank.getAttack();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getMaxMoveDistance Tank", "[Tank]") {
  SECTION("Getter getMaxMoveDistance() returns 4") {
    // Arrange
    const auto expected = 4;
    std::string newOwner = "Tamara";
    Tank tank(newOwner);

    // Act
    const auto result = tank.getMaxMoveDistance();

    // Assert
    REQUIRE(result == expected);
  }
}
TEST_CASE("getHealth Tank", "[Tank]") {
  SECTION("Getter getHealth() returns 10") {
    // Arrange
    const auto expected = 10;
    std::string newOwner = "Tamara";
    Tank tank(newOwner);

    // Act
    const auto result = tank.getHealth();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("getAttackReach Tank", "[Tank]") {
  SECTION("Getter getAttackReach() returns 1") {
    // Arrange
    const auto expected = 1;
    std::string newOwner = "Tamara";
    Tank tank(newOwner);

    // Act
    const auto result = tank.getAttackReach();

    // Assert
    REQUIRE(result == expected);
  }
}

TEST_CASE("specialAbility Tank", "[Tank]") {
  SECTION("When field type is clear, sets Tank attack strength to 5") {
    Tank tank("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Clear);

    const int expected = 5;

    tank.specialAbility(hex);

    int result = tank.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is forest, sets Tank attack strength to 3") {
    Tank tank("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Forest);

    const int expected = 3;

    tank.specialAbility(hex);

    int result = tank.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is city, sets Tank attack strength to 2") {
    Tank tank("Tina");
    Hex* hex = new Hex(1, 1, FieldType::City);

    const int expected = 2;

    tank.specialAbility(hex);

    int result = tank.getAttack();

    REQUIRE(result == expected);
  }
  SECTION("When field type is mountain, sets Tank attack strength to 4") {
    Tank tank("Tina");
    Hex* hex = new Hex(1, 1, FieldType::Mountain);

    const int expected = 4;

    tank.specialAbility(hex);

    int result = tank.getAttack();

    REQUIRE(result == expected);
  }
}
