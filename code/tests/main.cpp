#define CATCH_CONFIG_RUNNER
#include <tests/catch.hpp>
//#include <QtGui/QGuiApplication>
#include <QApplication>
#include "../headers/Game.h"

Game* game;

int main(int argc, char** argv) {
  //    QGuiApplication app(argc, argv);
  QApplication a(argc, argv);

  game = new Game();

  return Catch::Session().run(argc, argv);
}
