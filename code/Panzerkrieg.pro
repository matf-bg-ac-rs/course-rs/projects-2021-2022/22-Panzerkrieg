QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/Cannon.cpp \
    src/Cavalry.cpp \
    src/Game.cpp \
    src/HexBoard.cpp \
    src/Infantry.cpp \
    src/Player.cpp \
    src/Shop.cpp \
    src/State.cpp \
    src/Tank.cpp \
    src/TheArmy.cpp \
    src/client.cpp \
    src/exit.cpp \
    src/field.cpp \
    src/Hex.cpp \
    src/main.cpp \
    src/Map.cpp \
    src/main_game_window.cpp \
    src/playerlost.cpp \
    src/shopwindow.cpp \
    src/welcome.cpp \
    src/winner.cpp


HEADERS += \
    headers/Game.h \
    headers/HexBoard.h \
    headers/State.h \
    headers/client.h \
    headers/exit.h \
    headers/field.h \
    headers/Map.h \
    headers/Tank.h \
    headers/TheArmy.h \
    headers/Infantry.h \
    headers/Cannon.h \
    headers/Cavalry.h \
    headers/Player.h \
    headers/Shop.h \
    headers/Hex.h \
    headers/main_game_window.h \
    headers/playerlost.h \
    headers/shopwindow.h \
    headers/welcome.h \
    headers/winner.h

FORMS += \
    forms/exit.ui \
    forms/main_game_window.ui \
    forms/playerlost.ui \
    forms/shopwindow.ui \
    forms/welcome.ui \
    forms/winner.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


DISTFILES += \
    map1.txt \
    map2.txt \
    map3.txt \
    map_test.txt

RESOURCES += \
    ../Resources/resources.qrc \
    ../Resources/resources.qrc


