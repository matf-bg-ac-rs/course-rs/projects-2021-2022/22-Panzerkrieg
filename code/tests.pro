TEMPLATE = app
QT += gui
QT += widgets network

CONFIG += c++17

#isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
## set by Qt Creator wizard
#isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR="/home/jovan/Desktop/Razvoj softvera/projekat/22-Panzerkrieg/code"
#!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

#isEmpty(CATCH_INCLUDE_DIR): {
#    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
#}

SOURCES +=  \
        tests/main.cpp \
    tests/tst_map.cpp \
        tests/tst_testplayer.cpp \
        tests/tst_testfield.cpp \
        tests/tst_testcannon.cpp \
        tests/tst_testtank.cpp \
        tests/tst_testcavalry.cpp \
        tests/tst_testinfantry.cpp \
        tests/tst_testgame.cpp \
        tests/tst_testplayer.cpp \
        tests/tst_testshop.cpp\
        src/Cannon.cpp \
        src/Cavalry.cpp \
        src/Game.cpp \
        src/HexBoard.cpp \
        src/Infantry.cpp \
        src/Player.cpp \
        src/Shop.cpp \
        src/State.cpp \
        src/Tank.cpp \
        src/TheArmy.cpp \
        src/client.cpp \
        src/exit.cpp \
        src/field.cpp \
        src/Hex.cpp \
        src/main.cpp \
        src/Map.cpp \
        src/main_game_window.cpp \
        src/playerlost.cpp \
        src/shopwindow.cpp \
        src/welcome.cpp \
        src/winner.cpp

HEADERS += \
        tests/catch.hpp \
        headers/Game.h \
        headers/HexBoard.h \
        headers/State.h \
        headers/client.h \
        headers/exit.h \
        headers/field.h \
        headers/Map.h \
        headers/Tank.h \
        headers/TheArmy.h \
        headers/Infantry.h \
        headers/Cannon.h \
        headers/Cavalry.h \
        headers/Player.h \
        headers/Shop.h \
        headers/Hex.h \
        headers/main_game_window.h \
        headers/playerlost.h \
        headers/shopwindow.h \
        headers/welcome.h \
        headers/winner.h

FORMS += \
    forms/exit.ui \
    forms/main_game_window.ui \
    forms/playerlost.ui \
    forms/shopwindow.ui \
    forms/welcome.ui \
    forms/winner.ui

DISTFILES += \
    maps/map1.txt \
    maps/map2.txt \
    maps/map3.txt \
    maps/map_test.txt

RESOURCES += \
    ../Resources/resources.qrc \
    ../Resources/resources.qrc
