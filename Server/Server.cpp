#include "server.h"
#include <QRandomGenerator>
#include <iostream>

Server::Server(QObject* parent) : QObject(parent) {
  server = new QTcpServer(this);

  connect(server, &QTcpServer::newConnection, this, &Server::newConnection);

  int numOfMap = QRandomGenerator::global()->bounded(1, 4);
  this->path = "../code/maps/map" + std::to_string(numOfMap) + ".txt";

  if (!server->listen(QHostAddress::LocalHost, 1234)) {
    qDebug() << "Server couldn't start";
  } else {
    qDebug() << "Server started";
  }

  for (int i = 0; i < 4; i++)
    indexes.push_back(0);
  std::iota(indexes.begin(), indexes.end(), 0);
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(indexes.begin(), indexes.end(),
               std::default_random_engine(seed));

  connect(this, &Server::StartGame, this,
          [this](QTcpSocket* con, int numOfPlayers) {
            if (numOfPlayers != clients.size()) {
              sendToClient(con, "wrongNumPlayers");
            } else {
              player++;
              sendToClient(
                  con, QString::fromStdString(
                           "startGame " + std::to_string(player) + " " +
                           this->path + " " + std::to_string(indexes[0]) + " " +
                           std::to_string(indexes[1]) + " " +
                           std::to_string(indexes[2]) + " " +
                           std::to_string(indexes[3])));
            }
          });

  connect(this, &Server::EndTurn, this, [this]() {
    for (auto client : clients)
      sendToClient(client, QString::fromStdString("EndTurn"));
  });

  connect(this, &Server::PlayerShopped, this, [this](std::string army) {
    for (auto client : clients)
      sendToClient(client, QString::fromStdString("PlayerShopped " + army));
  });

  connect(this, &Server::MapPressed, this, [this](QString mess) {
    double a = QRandomGenerator::global()->generateDouble();
    double b = QRandomGenerator::global()->generateDouble();
    double c = QRandomGenerator::global()->generateDouble();
    double d = QRandomGenerator::global()->generateDouble();
    for (auto client : clients)
      sendToClient(client,
                   mess + QString::fromStdString(" " + std::to_string(a) + " " +
                                                 std::to_string(b) + " " +
                                                 std::to_string(c) + " " +
                                                 std::to_string(d)));
  });

  connect(this, &Server::PlayerQuit, this, [this](QString mes) {
    for (auto client : clients)
      sendToClient(client, mes);
  });
}

void Server::sendToClient(QTcpSocket* con, const QString& msg) {
  QByteArray data;
  QDataStream stream(&data, QIODevice::WriteOnly);

  stream << (quint32)0;
  stream << msg.toUtf8();
  stream.device()->seek(0);
  stream << (quint32)data.size();

  con->write(data);
}

void Server::readyRead() {
  if (QTcpSocket* con = qobject_cast<QTcpSocket*>(sender())) {
    QByteArray buffer;

    QDataStream socketStream(con);
    socketStream.setVersion(QDataStream::Qt_5_12);

    socketStream.startTransaction();
    socketStream >> buffer;

    if (!socketStream.commitTransaction()) {
      return;
    }

    QString message =
        QString("%1").arg(QString::fromStdString(buffer.toStdString()));

    readMessage(message, con);
  }
}

void Server::newConnection() {
  while (server->hasPendingConnections()) {
    QTcpSocket* con = server->nextPendingConnection();
    connect(con, &QTcpSocket::disconnected, this, &Server::disconnectClient);

    if (clients.size() <= 4) {
      connect(con, &QTcpSocket::readyRead, this, &Server::readyRead);
      clients.push_back(con);
    } else
      std::cout << "Too many players! The maximum number is four" << std::endl;

    std::cout << "New connection" << std::endl;
  }
}

void Server::disconnectClient() {
  if (QTcpSocket* con = qobject_cast<QTcpSocket*>(sender())) {
    std::vector<QTcpSocket*> new_clients(clients.size() - 1);
    remove_copy(clients.begin(), clients.end(), new_clients.begin(), con);
    clients = new_clients;
    std::cout << "Client disconnected" << std::endl;
    con->disconnect();
    con->disconnectFromHost();
    con->deleteLater();

    if (clients.size() == 0)
      player = 0;
  }
}

void Server::readMessage(const QString& message, QTcpSocket* con) {
  auto words = message.split(' ');
  auto mes = words[0];

  if (mes == "start") {
    emit StartGame(con, words[1].toInt());
  } else if (mes == "endTurn") {
    emit EndTurn();
  } else if (mes == "playerShopped") {
    emit PlayerShopped(words[1].toStdString());
  } else if (mes == "mapPressed") {
    emit MapPressed("MapPressed " + words[1] + " " + words[2]);
  } else if (mes == "playerQuit") {
    emit PlayerQuit("PlayerQuit " + words[1]);
  }
}
