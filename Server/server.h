#ifndef SERVER_H
#define SERVER_H

#include <QDebug>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class Server : public QObject {
  Q_OBJECT
 public:
  explicit Server(QObject* parent = 0);

 signals:
  void Connection(QTcpSocket* con);
  void StartGame(QTcpSocket* con, int);
  void EndTurn();
  void PlayerShopped(std::string);
  void MapPressed(QString);
  void PlayerQuit(QString);

 public slots:
  void newConnection();
  void readyRead();

 private:
  QTcpServer* server;
  std::vector<QTcpSocket*> clients;
  int count_clients = 0;
  void sendToClient(QTcpSocket* con, const QString& msg);
  void readMessage(const QString& message, QTcpSocket* con);
  void newMessage();
  void disconnectClient();
  int player = 0;
  std::string path;
  std::vector<int> indexes;
};

#endif  // SERVER_H
